-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 09, 2019 at 09:21 AM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fixasset_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_user_group`
--

CREATE TABLE `t_user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `level_approve` int(50) NOT NULL,
  `approve_min` int(50) NOT NULL,
  `approve_max` int(50) NOT NULL,
  `assign_approver` int(10) NOT NULL,
  `approver_user_id` int(11) NOT NULL,
  `create_user` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(50) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user_group`
--

INSERT INTO `t_user_group` (`id`, `group_name`, `level_approve`, `approve_min`, `approve_max`, `assign_approver`, `approver_user_id`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 'Cost Center Admin', 1, 0, 0, 1, 4, '', '2019-02-06 00:00:00', 'admin', '2019-02-09 08:42:37', 0),
(2, 'Cost Center Manager Level 1', 11112, 12, 12, 0, 0, 'admin', '2019-02-06 16:10:01', 'admin', '2019-02-08 08:03:51', 0),
(3, 'Cost Center Manager Level 2', 123, 0, 0, 0, 0, 'admin', '2019-02-06 16:28:48', 'admin', '2019-02-09 08:59:58', 0),
(4, 'Admin Manager', 0, 0, 0, 1, 5, 'admin', '2019-02-08 04:18:01', 'admin', '2019-02-09 09:13:54', 0),
(5, 'Finance Mangager', 0, 0, 0, 1, 8, 'admin', '2019-02-08 04:18:33', 'admin', '2019-02-09 09:20:13', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_user_group`
--
ALTER TABLE `t_user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_user_group`
--
ALTER TABLE `t_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
