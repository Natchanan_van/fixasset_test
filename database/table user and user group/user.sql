-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2019 at 04:34 AM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fixasset_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `cost_center_code` varchar(30) NOT NULL,
  `user` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(3) NOT NULL,
  `create_user` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(60) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `company_id`, `cost_center_code`, `user`, `password`, `email`, `role`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 2, '1190102', 'admin', 'd9b1d7db4cd6e70935368a1efb10e377', 'akarapon.n@gmail.com', 1, 'test', '2019-01-16 00:00:00', 'admin', '2019-02-06 17:32:44', 0),
(2, 2, '1101101', 'test', '24a1a3bc832644156b357026cbca787e', 'test', 2, 'admin', '2019-02-06 17:15:57', 'admin', '2019-02-06 18:11:07', 0),
(3, 1, '1136101', 'cest', '611928cf470caa02a43e91501ddbe642', 'cest', 3, 'admin', '2019-02-06 17:16:17', 'admin', '2019-02-06 18:09:33', 0),
(4, 1, '1101101', 'pap', '4e951936957c783062a399c629ce9a95', 'pap', 3, 'admin', '2019-02-06 18:37:35', '', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
