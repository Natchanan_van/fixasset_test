-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2019 at 04:40 AM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fixasset_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_user_group`
--

CREATE TABLE `t_user_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `level_approve` int(50) NOT NULL,
  `approve_min` int(50) NOT NULL,
  `approve_max` int(50) NOT NULL,
  `create_user` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(50) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user_group`
--

INSERT INTO `t_user_group` (`id`, `group_name`, `level_approve`, `approve_min`, `approve_max`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 'Cost Center Admin', 1, 0, 0, '', '2019-02-06 00:00:00', 'admin', '2019-02-06 17:10:17', 0),
(2, 'test', 11112, 12, 12, 'admin', '2019-02-06 16:10:01', 'admin', '2019-02-06 17:10:26', 0),
(3, 'test2', 123, 0, 0, 'admin', '2019-02-06 16:28:48', 'admin', '2019-02-06 18:41:44', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_user_group`
--
ALTER TABLE `t_user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_user_group`
--
ALTER TABLE `t_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
