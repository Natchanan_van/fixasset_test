myApp.controller('userController', ['$scope', '$filter',  'baseService', 'userApiService', 'companyApiService','costcenterApiService','userGroupApiService',  function ($scope, $filter,  baseService, userApiService, companyApiService, costcenterApiService, userGroupApiService ) {
    $scope.CreateModel = {};
	$scope.modelSearch = {};
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('user_id');
    $scope.SortOrder = baseService.setSortOrder('asc');
	
	$scope.listUser = [];
	$scope.listCompany =[];
	$scope.listCostcenter =[];
	$scope.listUserGroup =[];
	$scope.TempUserIndex = {};
	$scope.TempCostcenterIndex ={};
	$scope.TempSearchUserIndex = {};
	$scope.TempUserGroupIndex ={};
	$scope.TempCompanyIndex ={};
    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

     $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function (item) {
        $scope.resetModel (); 
		if(item == undefined){
			// console.log("testadd");
			userApiService.loadUserid(null, function (results) {  
				
				$scope.CreateModel.user_id = results[0]['user_id'];
				// console.log($scope.CreateModel);
			})
			 
				
		
				
		
		}
		 
		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) {  
        $scope.AddNewDevice(item); 
		$scope.loadEditData(item);              
    }
	
    $scope.loadEditData = function (item) { 
		$scope.CreateModel.user_id =  item.user_id;
		$scope.CreateModel.user_name =  item.user;
		$scope.CreateModel.user_password =  item.password;
		$scope.CreateModel.user_email =  item.email;
		
		if($scope.listCompany.length > 0){ 
			$scope.listCompany.forEach(function (entry, index) {
				// console.log(item.company_id);
				if (item.company_id === entry.company_id)
					$scope.TempCompanyIndex.selected = entry;
					
			});
		}
		if($scope.listCostcenter.length > 0){ 
			$scope.listCostcenter.forEach(function (entry, index) {
				// console.log(item.cost_center_code);
				if (item.cost_center_code === entry.cost_code)
				
				$scope.TempCostcenterIndex.selected = entry;
				
			});
		}
		if($scope.listUserGroup.length > 0){ 
			$scope.listUserGroup.forEach(function (entry, index) {
				if (item.role === entry.id)
					$scope.TempUserGroupIndex.selected = entry;
			});
		}
	}
	
	$scope.resetModel = function () {

        $scope.CreateModel = { user_id:0,  user_name : "", user_password: "", user_email:"" };
		 
		 if($scope.listCompany.length > 0){
			$scope.TempCompanyIndex.selected =  $scope.listCompany[0]; 
		 }
		 if($scope.listCostcenter.length > 0){
			$scope.TempCostcenterIndex.selected =  $scope.listCostcenter[0]; 
		 }
		 if($scope.listUserGroup.length > 0){
			$scope.TempUserGroupIndex.selected =  $scope.listUserGroup[0]; 
		 }
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"user_name": "",
							"cost_cent_code":"",
							
						};
						
		// $scope.TempSearchProjectIndex = {};
		// $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
	$scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		
		
		
	
		costcenterApiService.getComboBox(null, function(result){
		
			// var result = results.data;
			// console.log(results);
			// console.log(results.data);
			if(true == result.status){
				 $scope.listCostcenter =  result.message
				
				 console.log($scope.listCostcenter); 
				 if($scope.listCostcenter.length > 0){
					$scope.TempCostcenterIndex.selected =  $scope.listCostcenter[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		userGroupApiService.getComboBox(null, function(result){
		
			// var result = results.data;
			// console.log(results);
			// console.log(result);
			if(true == result.status){
				 $scope.listUserGroup =  result.message
				
			// 	 console.log($scope.listCostcenter); 
				 if($scope.listUserGroup.length > 0){
					$scope.TempUserGroupIndex.selected =  $scope.listUserGroup[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		
			
			 
			 
		

		companyApiService.getComboBox(null, function(result){
		
			// var result = results.data;
			
			if(true == result.status){
				 $scope.listCompany =  result.message
				
				 console.log($scope.listCompany); 
				 if($scope.listCompany.length > 0){
					$scope.TempCompanyIndex.selected =  $scope.listCompany[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}

		});
		

        // $scope.listPageSize.forEach(function (entry, index) {
        //     if (0 === index)
        //         $scope.TempPageSize.selected = entry;
        // });
		
		$scope.reload();
    }
	
	$scope.reload = function (){
		 
		// if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
		// 	$scope.modelSearch.pro_id = 0;
		// }else{
		// 	$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		// }
		// console.log($scope.modelSearch);
		userApiService.listUser($scope.modelSearch, function (results) {  
            
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.totalPage;// console.log($scope.toTalPage);
				$scope.listPageIndex = baseService.getListPage(result.totalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.listUser = result.message;
                console.log($scope.listUser);
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		
		
		userApiService.deleteUser({user_id:item.user_id}, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}
	

	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		
		  
		if($scope.CreateModel.item == ""){
			$(".require").show();
			bResult = false;
		}
		if($scope.CreateModel.user_name == ""){
			$(".CreateModel_user").show();
			bResult = false;
		}
		if($scope.CreateModel.user_password == ""){
			$(".CreateModel_password").show();
			bResult = false;
		}
		if($scope.CreateModel.user_email == ""){
			$(".CreateModel_email").show();
			bResult = false;
		}
		
		// if(($scope.CreateModel.budget == "" && $scope.CreateModel.budget != 0) || $scope.CreateModel.budget == null){
			// $(".CreateModel_budget").show();
			// bResult = false;
		// }
	 
		// if($scope.CreateModel.price <= 0){
		// 	$(".CreateModel_price").show();
		// 	bResult = false;
		// }
		
		// $scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
		// $scope.CreateModel.pro_name = $scope.TempProjectIndex.selected.name;
		
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {
       var bValid = $scope.validatecheck();
	   $scope.CreateModel.company_id = $scope.TempCompanyIndex.selected.company_id;
	   $scope.CreateModel.cost_code = $scope.TempCostcenterIndex.selected.cost_code;
	   $scope.CreateModel.role = $scope.TempUserGroupIndex.selected.id;
	//    console.log($scope.CreateModel.role);
		if(true == bValid){
			userApiService.saveUser($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });
		}
            
		// }
		
    }


}]); 