myApp.controller('vatController', ['$scope', '$filter',  'baseService', 'vatApiService','systemApiService', 'projectApiService', function ($scope, $filter,  baseService, vatApiService , systemApiService, projectApiService) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
	$scope.CompanyModel = {};
	$scope.fullset = false;
	
	$scope.listProject = [];
	$scope.TempProjectIndex  = {}; 
	$scope.TempSearchProjectIndex  = {}; 
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
		$scope.fullset = false;
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
         $scope.resetModel (); 
		 $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
		$scope.setCompanyData();
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice();   
		$scope.loadEditData(item);
             
    }
	
    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item); 
		
		if($scope.listProject.length > 0){ 
			$scope.listProject.forEach(function (entry, index) {
				 		
				if (entry.id === item.pro_id){
					$scope.TempProjectIndex.selected =  entry; 
				} 
			});
		} 
	}
	
	$scope.getTaxId = function (taxid){
		try{
			taxid = taxid.replace(/\s+/g, '');
			taxid = taxid.substring(0, 13);
		}catch(e){
			
		}
		
		return taxid;
	}
	
	$scope.setCompanyData = function(){
		  
		$scope.CreateModel.pay_name = $scope.CompanyModel.name;
		$scope.CreateModel.pay_id = $scope.getTaxId ($scope.CompanyModel.taxid); 
		$scope.CreateModel.pay_address = $scope.CompanyModel.address1 + "\n" +  $scope.CompanyModel.address2 + "\n" +  $scope.CompanyModel.address3;
	}
	
	$scope.resetModel = function () { 
        $scope.CreateModel = { id: 0, book_no: "", num_no: "", pay_name: "", pay_id: "", pay_address: "", com_name: "", com_id: "", com_address: "", salary_year: "", salary_pay: "", salary_wht: "", fee_year: "", fee_pay: "", fee_wht: "", copyfee_year: "", copyfee_pay: "", copyfee_wht: "", interest_year: "", interest_pay: "", interest_wht: "", interest1_year: "", interest1_pay: "", interest1_wht: "", interest2_year: "", interest2_pay: "", interest2_wht: "", interest3_year: "", interest3_pay: "", interest3_wht: "", interest4_year: "", interest4_pay: "", interest4_wht: "", interest21_year: "", interest21_pay: "", interest21_wht: "", interest22_year: "", interest22_pay: "", interest22_wht: "", interest23_year: "", interest23_pay: "", interest23_wht: "", interest24_year: "", interest24_pay: "", interest24_wht: "", interest25_year: "", interest25_pay: "", interest25_wht: "", interest25_comment: "", revenue_year: "", revenue_pay: "", revenue_wht: "", revenue_comment: "", other_year: "", other_pay: "", other_wht: "", other_comment: "", total_pay: "", total_wht: "", total_alphabet: "", onetime_vat: "", forever_vat: "", wht_vat: "", other_vat: "", other_vat_comment: "", sign_name: "", sign_date : new Date()  };
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"NumNo": "",
							"CompanyNameEx2": "" 
						}; 
						
		$scope.TempSearchProjectIndex  = {}; 
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });
		
			
		systemApiService.getCompany(null, function (result) {  
			if(result.status === true){ 
				$scope.CompanyModel = result.message[0];
				$scope.setCompanyData();
			}else{
				baseService.showMessage(result.message);
			}
		});
		
		projectApiService.getComboBox(null, function(results){
			
			var result = results.data;
			
			if(true == result.status){
				 $scope.listProject =  result.message
				 
				 if($scope.listProject.length > 0){
					$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		
		//$scope.reload();
    }
	
	$scope.reload = function (){
		
		if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
			$scope.modelSearch.pro_id = 0;
		}else{
			$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		}
		
		vatApiService.listvat($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
				$scope.modelDeviceList = result.message; 
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		vatApiService.deletevat({id:item.id}, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}
	
	$scope.loadAlphabet = function(){
		$scope.CreateModel.total_alphabet = ThaiNumberToText($scope.CreateModel.total_wht);
	}
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.book_no == ""){
			$(".CreateModel_book_no").show();
			bResult = false;
		}
		
		if($scope.CreateModel.num_no == ""){
			$(".CreateModel_num_no").show();
			bResult = false;
		}
		
		if($scope.CreateModel.pay_name == ""){
			$(".CreateModel_pay_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.pay_id == ""){
			$(".CreateModel_pay_id").show();
			bResult = false;
		}
		if($scope.CreateModel.com_name == ""){
			$(".CreateModel_com_name").show();
			bResult = false;
		}
		if($scope.CreateModel.com_id == ""){
			$(".CreateModel_com_id").show();
			bResult = false;
		}
		if($scope.CreateModel.com_address == ""){
			$(".CreateModel_com_address").show();
			bResult = false;
		}
		
		if($scope.CreateModel.total_pay == ""){
			$(".CreateModel_total_pay").show();
			bResult = false;
		}
		if($scope.CreateModel.total_wht == ""){
			$(".CreateModel_total_wht").show();
			bResult = false;
		}
		if($scope.CreateModel.total_alphabet == ""){
			$(".CreateModel_total_alphabet").show();
			bResult = false;
		}
		
		
		
		if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.note == ""){
			$(".CreateModel_note").show();
			bResult = false;
		}
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
     
        var bValid = $scope.validatecheck();
		
		$scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
	  
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            vatApiService.savevat($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
    }
	
	$scope.printPDF = function(item){ 
		var urlName = get_base_url("vat/printPDF?id=" + item.id);
		window.open(urlName);
	}

}]); 