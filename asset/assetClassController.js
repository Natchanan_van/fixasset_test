myApp.controller('assetClassController', ['$scope', '$filter',  'baseService', 'assetclassApiService','companyApiService',
                function ($scope, $filter,  baseService,assetclassApiService,companyApiService) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
    
    // $scope.listCompany = [];
	// $scope.TempCompanyIndex = {};
	// $scope.TempSearchCompanyIndex = {};


	
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('class_id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function (item) {

         $scope.resetModel (); 

         if(item == undefined){
			console.log("testadd");
			assetclassApiService.loadClassid(null, function (results) {  
				
				$scope.CreateModel.class_id = results[0]['class_id'];
				console.log($scope.CreateModel);
			})
		}

		 $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice(item);   
		$scope.loadEditData(item);
             
    }
	
    $scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item); 
		
		console.log($scope.CreateModel);
	
	 
			// if($scope.listCompany.length > 0 ){
			// $scope.listCompany.forEach(function (entry, index) {    
			// 	if(entry.company_id == item.company_id){  
			// 		$scope.TempCompanyIndex.selected  = entry;
			// 			}
			// 	});
			// 	}
	}
	
	$scope.resetModel = function () {

        // $scope.CreateModel = { id:0, code:"", name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", website: ""};
		$scope.CreateModel = { class_id:0,class_code:"",class_description:"",it_asset:0 };
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							// "code": "",
							// "name": "",
                            // "contact": "",
                         
							"class_code": "",
							"class_description": ""
						}; 
        // $scope.TempSearchCompanyIndex.selected = "";
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
		});
        // companyApiService.getComboBox(null, function(results){ 
		// 	if(true == results.status){ 
		// 		 $scope.listCompany =  results.message;
		// 		//  console.log("TEST");
		// 		//  console.log($scope.listAccount);
		// 		 $scope.listCompany.forEach(function (entry, index) {
			
		// 			if ($scope.CreateModel.company_id === entry.company_id){
		// 				$scope.TempCompanyIndex.selected = entry;
		// 			}
		// 		}); 
				
		// 	}else{
		// 		baseService.showMessage(results.message);
		// 	}
		// })
		//$scope.reload();
    }
	
	$scope.reload = function (){


		// if("undefined" == typeof $scope.TempSearchCompanyIndex.selected){
		// 	$scope.modelSearch.company_id = "";
		// }else{
		// 	$scope.modelSearch.company_id = $scope.TempSearchCompanyIndex.selected.company_id;
		// }
		// console.log('run reload');
		console.log($scope.modelSearch);
		assetclassApiService.listAssetClass($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.modelDeviceList = result.message; 
            //    console.log($scope.modelDeviceList);
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		// console.log('del');
		assetclassApiService.deleteAssetClass(item, function (result) {  
                if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

    }
   
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.name == ""){
			$(".CreateModel_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.code == ""){
			$(".CreateModel_code").show();
			bResult = false;
		}
		
		if($scope.CreateModel.contact == ""){
			$(".CreateModel_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.taxid == ""){
			$(".CreateModel_taxid").show();
			bResult = false;
		}
		if($scope.CreateModel.tel == ""){
			$(".CreateModel_tel").show();
			bResult = false;
		}
		if($scope.CreateModel.email == ""){
			$(".CreateModel_email").show();
			bResult = false;
		}
		if($scope.CreateModel.website == ""){
			$(".CreateModel_website").show();
			bResult = false;
		}
		
		if($scope.CreateModel.address1 == ""){
			$(".CreateModel_address1").show();
			bResult = false;
		}
		if($scope.CreateModel.address2 == ""){
			$(".CreateModel_address2").show();
			bResult = false;
		}
		if($scope.CreateModel.address3 == ""){
			$(".CreateModel_address3").show();
			bResult = false;
		}
		
		
		
		if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.note == ""){
			$(".CreateModel_note").show();
			bResult = false;
		}
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
     
		var bValid = $scope.validatecheck();
		

		console.log($scope.CreateModel);
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            assetclassApiService.saveAssetClass($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
    }


}]); 