myApp.controller('countAssetController', ['$scope', '$filter',  'baseService', 'countAssetApiService','companyApiService','costcenterApiService','submitStatusApiService',
                function ($scope, $filter,  baseService,countAssetApiService,companyApiService,costcenterApiService,submitStatusApiService) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
	$scope.listData = {}; 
    
    $scope.countAssetDetail = {};
    
    $scope.listCompany = [];
	$scope.TempCompanyIndex = {};
	$scope.TempSearchCompanyIndex = {};

	$scope.listCostCenter = [];
	$scope.TempCostCenterIndex = {};
	$scope.TempSearchCostCenterIndex = {};

	$scope.listSubmitStatus = [];
	$scope.TempSubmitStatusIndex = {};
	$scope.TempSearchSubmitStatusIndex = {};


    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('count_no');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function (item) {

         $scope.resetModel (); 

         if(item == undefined){
			// console.log("testadd");
			accountApiService.loadAccountid(null, function (results) {  
				
				$scope.CreateModel.account_id = results[0]['account_id'];
				console.log($scope.CreateModel);
			})
		}

		 $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice(item);   
		$scope.loadEditData(item);
             
    }
	
    $scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item); 
		
		// console.log($scope.CreateModel);
	
        countAssetApiService.listCountAssetDetail($scope.CreateModel, function (results) {  
            console.log(results);
             
            $scope.countAssetDetail = results.message; 
           console.log($scope.countAssetDetail);
      
         })
	}
	
	$scope.resetModel = function () {

        // $scope.CreateModel = { id:0, code:"", name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", website: ""};
		$scope.CreateModel = { account_id:0,account_code:"",account_name:"" };
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							// "code": "",
							// "name": "",
                            // "contact": "",
							"countdate_start": "",
							"countdate_end":"",
							"cost_id": "",
							"company_id":"",
							"submit_status":""
						}; 
		$scope.TempSearchCompanyIndex.selected = "";
		$scope.TempSearchCostCenterIndex.selected = "";
		$scope.TempSearchSubmitStatusIndex.selected = "";
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
		});
		costcenterApiService.getComboBox(null, function(results){ 
			if(true == results.status){ 
				 $scope.listCostCenter =  results.message;
				//  console.log("TEST");
				//  console.log($scope.listCostCenter);
				 $scope.listCostCenter.forEach(function (entry, index) {
			
					if ($scope.CreateModel.cost_id === entry.cost_id){
						$scope.TempCostCenterIndex.selected = entry;
					}
				}); 
				
			}else{
				baseService.showMessage(results.message);
			}
		}),
        companyApiService.getComboBox(null, function(results){ 
			if(true == results.status){ 
				 $scope.listCompany =  results.message;
				//  console.log("TEST");
				//  console.log($scope.listAccount);
				 $scope.listCompany.forEach(function (entry, index) {
			
					if ($scope.CreateModel.company_id === entry.company_id){
						$scope.TempCompanyIndex.selected = entry;
					}
				}); 
				
			}else{
				baseService.showMessage(results.message);
			}
		}),
		submitStatusApiService.getComboBox(null, function(results){ 
			if(true == results.status){ 
				 $scope.listSubmitStatus =  results.message;
				//  console.log("TEST");
				//  console.log($scope.listSubmitStatus);
				 $scope.listSubmitStatus.forEach(function (entry, index) {
			
					if ($scope.CreateModel.submit_id === entry.submit_id){
						$scope.TempSubmitStatusIndex.selected = entry;
					}
				}); 
				
			}else{
				baseService.showMessage(results.message);
			}
		})
		//$scope.reload();
    }
	
	$scope.reload = function (){


		if("undefined" == typeof $scope.TempSearchCompanyIndex.selected){
			$scope.modelSearch.company_id = "";
		}else{
			$scope.modelSearch.company_id = $scope.TempSearchCompanyIndex.selected.company_id;
		}

		if("undefined" == typeof $scope.TempSearchCostCenterIndex.selected){
			$scope.modelSearch.cost_id = "";
		}else{
			$scope.modelSearch.cost_id = $scope.TempSearchCostCenterIndex.selected.cost_id;
		}

		
		if("undefined" == typeof $scope.TempSearchSubmitStatusIndex.selected){
			$scope.modelSearch.submit_status = "";
		}else{
			$scope.modelSearch.submit_status = $scope.TempSearchSubmitStatusIndex.selected.submit_id;
		}
		// console.log($scope.TempSearchSubmitStatusIndex.selected);
		console.log($scope.modelSearch);
		countAssetApiService.listCountAsset($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.modelDeviceList = result.message; 
               console.log($scope.modelDeviceList);
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		// console.log('del');
		accountApiService.deleteAccount(item, function (result) {  
                if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

    }
   
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.name == ""){
			$(".CreateModel_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.code == ""){
			$(".CreateModel_code").show();
			bResult = false;
		}
		
		if($scope.CreateModel.contact == ""){
			$(".CreateModel_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.taxid == ""){
			$(".CreateModel_taxid").show();
			bResult = false;
		}
		if($scope.CreateModel.tel == ""){
			$(".CreateModel_tel").show();
			bResult = false;
		}
		if($scope.CreateModel.email == ""){
			$(".CreateModel_email").show();
			bResult = false;
		}
		if($scope.CreateModel.website == ""){
			$(".CreateModel_website").show();
			bResult = false;
		}
		
		if($scope.CreateModel.address1 == ""){
			$(".CreateModel_address1").show();
			bResult = false;
		}
		if($scope.CreateModel.address2 == ""){
			$(".CreateModel_address2").show();
			bResult = false;
		}
		if($scope.CreateModel.address3 == ""){
			$(".CreateModel_address3").show();
			bResult = false;
		}
		
		
		
		if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.note == ""){
			$(".CreateModel_note").show();
			bResult = false;
		}
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
     
		var bValid = $scope.validatecheck();
		

		console.log($scope.CreateModel);
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            accountApiService.saveAccount($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
    }


}]); 