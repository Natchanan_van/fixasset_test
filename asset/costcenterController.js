myApp.controller('costcenterController', ['$scope', '$filter',  'baseService', 'costcenterApiService','companyApiService','userApiService',
                function ($scope, $filter,  baseService,costcenterApiService,companyApiService,userApiService) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
    
    $scope.listCompany = [];
	$scope.TempCompanyIndex = {};
	$scope.TempSearchCompanyIndex = {};

	$scope.listUserlv1 = [];
	$scope.TempUserlv1Index = {};
	$scope.TempSearchUserlv1Index = {};

	$scope.listUserlv2 = [];
	$scope.TempUserlv2Index = {};
	$scope.TempSearchUserlv2Index = {};

	$scope.listcostcenter = [];
	$scope.TempcostcenterIndex = {};
	$scope.TempSearchcostcenterIndex = {};


	
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('cost_id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function (item) {

         $scope.resetModel (); 

         if(item == undefined){
			// console.log("testadd");
			costcenterApiService.loadCostid(null, function (results) {  
				
				$scope.CreateModel.cost_id = results[0]['cost_id'];
				// console.log($scope.CreateModel);
			})
		}

		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice(item);   
		$scope.loadEditData(item);
             
    }
	
    $scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item); 
		
		// console.log(item);
	
	 
			if($scope.listCompany.length > 0 ){
			$scope.listCompany.forEach(function (entry, index) {    
				if(entry.company_id === item.company_id){  
					$scope.TempCompanyIndex.selected  = entry;
					}
					// console.log($scope.TempCompanyIndex.selected);
				});
				}
			// if($scope.listUserlv1.length > 0 ){
			// 	$scope.listUserlv1.forEach(function (entry, index) {    
			// 		if(entry.user_id === item.approver_level1){  
			// 			$scope.TempUserlv1Index.selected  = entry;
			// 			}
			// 			// console.log(entry);
			// 		});
			// 		}
			// if($scope.listUserlv2.length > 0 ){
			// 	$scope.listUserlv2.forEach(function (entry, index) {    
			// 		if(entry.user_id === item.approver_level2){  
			// 			$scope.TempUserlv2Index.selected  = entry;
			// 			}
			// 		});
			// 		}
					
					userApiService.getComboBoxlv1($scope.CreateModel,function(result){
						// var result= results.data; 
						// console.log(result);
						if(result.status === true){ 
							console.log(result);
							$scope.listUserlv1 = result.message;
							
							$scope.listUserlv1.forEach(function (entry, index) {
								if (item.cost_code === entry.cost_center_code){
									$scope.TempUserlv1Index.selected = entry;
							}
							});
						}else{
								baseService.showMessage(result.message);
							}
							
				})

				userApiService.getComboBoxlv2($scope.CreateModel,function(result){
					// var result= results.data; 
					// console.log(result);
					if(result.status === true){ 
						console.log(result);
						$scope.listUserlv2 = result.message;
						
						$scope.listUserlv2.forEach(function (entry, index) {
							if (item.cost_code === entry.cost_center_code){
								$scope.TempUserlv2Index.selected = entry;
						}
						});
					}else{
							baseService.showMessage(result.message);
						}
						
			})
			}
		
	
	$scope.resetModel = function () {

        // $scope.CreateModel = { id:0, code:"", name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", website: ""};
		$scope.CreateModel = { cost_id:0,company_id:0,cost_code:"",cost_description:""};
		
		if($scope.listUserlv1.length > 0){
			$scope.TempUserlv1Index.selected =  $scope.listUserlv1[0]; 
		 }
		 if($scope.listUserlv2.length > 0){
			$scope.TempUserlv2Index.selected =  $scope.listUserlv2[0]; 
		 }
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							// "code": "",
							// "name": "",
                            // "contact": "",
                         
							"cost_code": "",
							"cost_description": "",
							"company_id":""
						}; 
        $scope.TempSearchCompanyIndex.selected = "";
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.reloadUser = function(){
		// var bValid=true;
		// $scope.CreateModel.cost_code = $scope.TempcostcenterIndex.cost_id;

		userApiService.getComboBoxlv1($scope.CreateModel,function(result){
			// console.log(result);
			if(true == result.status){
				$scope.listUserlv1 =  result.message
				if($scope.listUserlv1.length > 0){
					$scope.TempUserlv1Index.selected =  $scope.listUserlv1[0]; 
					// $scope.CreateModel.approver_level1 = parseFloat($scope.TempUserlv1Index.selected.user_id);
			}
		}else{
			baseService.showMessage(result.message);
		}
		})

		userApiService.getComboBoxlv2($scope.CreateModel,function(result){
			// console.log(result);
			if(true == result.status){
				$scope.listUserlv2 =  result.message
				if($scope.listUserlv2.length > 0){
					$scope.TempUserlv1Index.selected =  $scope.listUserlv2[0]; 
					// $scope.CreateModel.approver_level1 = parseFloat($scope.TempUserlv1Index.selected.user_id);
			}
		}else{
			baseService.showMessage(result.message);
		}
		})
	}

    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
		});
        companyApiService.getComboBox(null, function(results){ 
			if(true == results.status){ 
				 $scope.listCompany =  results.message;
				//  console.log("TEST");
				//  console.log($scope.listCompany);
				 $scope.listCompany.forEach(function (entry, index) {
			
					if ($scope.CreateModel.company_id === entry.company_id){
						$scope.TempCompanyIndex.selected = entry;
					}
				}); 
				
			}else{
				baseService.showMessage(results.message);
			}
		});

		userApiService.getComboBoxlv1(null, function(result){
		
			// var result = results.data;
			// console.log(results);
			// console.log(result);
			if(true == result.status){
				 $scope.listUserlv1 =  result.message
				
				//  console.log($scope.listUser); 
				if($scope.listUserlv1.length > 0){
					$scope.TempUserlv1Index.selected =  $scope.listUserlv1[0]; 
				}
				 
			}else{
				baseService.showMessage(result.message);
			}
		});

		userApiService.getComboBoxlv2(null, function(result){
		
			// var result = results.data;
			// console.log(results);
			// console.log(result);
			if(true == result.status){
				 $scope.listUserlv2 =  result.message
				
				//  console.log($scope.listUser); 
				if($scope.listUserlv2.length > 0){
					$scope.TempUserlv2Index.selected =  $scope.listUserlv2[0]; 
				}
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		$scope.reload();
    }
	
	$scope.reload = function (){


		if("undefined" == typeof $scope.TempSearchCompanyIndex.selected){
			$scope.modelSearch.company_id = "";
		}else{
			$scope.modelSearch.company_id = $scope.TempSearchCompanyIndex.selected.company_id;
		}
		// console.log('run reload');
		// console.log($scope.modelSearch);
		costcenterApiService.listCostcenter($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.modelDeviceList = result.message; 
            //    console.log($scope.modelDeviceList);
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		// console.log('del');
		costcenterApiService.deleteCostCenter(item, function (result) {  
                if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

    }
   
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.name == ""){
			$(".CreateModel_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.code == ""){
			$(".CreateModel_code").show();
			bResult = false;
		}
		
		if($scope.CreateModel.contact == ""){
			$(".CreateModel_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.taxid == ""){
			$(".CreateModel_taxid").show();
			bResult = false;
		}
		if($scope.CreateModel.tel == ""){
			$(".CreateModel_tel").show();
			bResult = false;
		}
		if($scope.CreateModel.email == ""){
			$(".CreateModel_email").show();
			bResult = false;
		}
		if($scope.CreateModel.website == ""){
			$(".CreateModel_website").show();
			bResult = false;
		}
		
		if($scope.CreateModel.address1 == ""){
			$(".CreateModel_address1").show();
			bResult = false;
		}
		if($scope.CreateModel.address2 == ""){
			$(".CreateModel_address2").show();
			bResult = false;
		}
		if($scope.CreateModel.address3 == ""){
			$(".CreateModel_address3").show();
			bResult = false;
		}
		
		
		
		if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.note == ""){
			$(".CreateModel_note").show();
			bResult = false;
		}
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
     
		var bValid = $scope.validatecheck();
		
		$scope.CreateModel.company_id = $scope.TempCompanyIndex.selected.company_id;
		$scope.CreateModel.approver_level1 = $scope.TempUserlv1Index.selected.user_id;
		$scope.CreateModel.approver_level2 = $scope.TempUserlv2Index.selected.user_id;

		console.log($scope.CreateModel);
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            costcenterApiService.saveCostCenter($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
    }


}]); 