myApp.controller('quatationController', ['$scope', '$filter',  'baseService', 'customerApiService', 'systemApiService', 'quatationApiService', 'projectApiService', function ($scope, $filter,  baseService, customerApiService, systemApiService, quatationApiService, projectApiService) {
    $scope.CreateModel = {};
	$scope.CompanyModel = {};
	$scope.QuatationDetailModel = [];

	$scope.listCustomer = [];
	$scope.TempCustomerIndex  = {};
	
	$scope.listProject = [];
	$scope.TempProjectIndex  = {}; 
	
	$scope.listStatus = [];
	$scope.TempStatusIndex  = {}; 
	$scope.TempStatusId = 0;
	
	$scope.TempSearchCustomerIndex  = {}; 
	$scope.TempSearchProjectIndex  = {}; 
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('IssueDate');
    $scope.SortOrder = baseService.setSortOrder('desc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
        $scope.resetModel (); 
		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
		$scope.QuatationDetailModel = [];
		$scope.setCompanyData();
		$scope.addQuatationDetail();
		$scope.loadFromCustomer();
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice();   
		$scope.loadEditData(item); 
    }
	
    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item);
		
		if($scope.listCustomer.length > 0){ 
			$scope.listCustomer.forEach(function (entry, index) {
				  			
				if (entry.id === item.cus_id){
					$scope.TempCustomerIndex.selected =  entry;
					//$scope.loadFromCustomer();
				} 
			});
		}
		
		if($scope.listProject.length > 0){ 
			$scope.listProject.forEach(function (entry, index) {
				 		
				if (entry.id === item.pro_id){
					$scope.TempProjectIndex.selected =  entry; 
				} 
			});
		} 
		
		//load quatation.. detail
		quatationApiService.listquatationDetail({id:item.id}, function(result){
			 
			if(true == result.status){
				$scope.QuatationDetailModel =  result.message
				  
			}else{
				baseService.showMessage(result.message);
			}
		});
		
				 
	}
	
	$scope.loadStatusView = function(item){
		
		var viewStatus = "กำลังดำเนินการ";
		
		if(item.status == 2){
			 viewStatus = "ยกเลิก";
		}else if(item.status == 3){
			 viewStatus = "อนุมัติ";
		}
		
		return viewStatus;
	}
	
	$scope.loadStatusCss = function(item){
		
		var viewStatus = "text-primary";
		
		if(item.status == 2){
			 viewStatus = "text-danger";
		}else if(item.status == 3){
			 viewStatus = "text-success";
		}
		
		return viewStatus;
	}
	 
	$scope.loadAmount = function(){
		
		$scope.CreateModel.sub_total = 0;
		$scope.QuatationDetailModel.forEach(function (entry, index) {
             try{
				 var price = parseFloat(entry.amount.replace(/,/g, '')); 
				 if(isNaN(price)) {
					 entry.amount = 0.00;
				 }else{
					entry.amount = price.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
					$scope.CreateModel.sub_total += price;
				 } 
			 }catch(e){
				 entry.amount = 0.00;
				// console.log(e);
			}
        });
		$scope.CreateModel.vat = $scope.CreateModel.sub_total * (0.07);
		$scope.CreateModel.sub_total = $scope.CreateModel.sub_total.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
		$scope.CreateModel.vat = $scope.CreateModel.vat.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
		$scope.CreateModel.total = parseFloat($scope.CreateModel.sub_total.replace(/,/g, '')) + parseFloat($scope.CreateModel.vat.replace(/,/g, '')) ;
		$scope.CreateModel.total = $scope.CreateModel.total.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
		$scope.CreateModel.sub_alphabet = ThaiNumberToText($scope.CreateModel.total);
	}
	
	$scope.resetModel = function () { 
        $scope.CreateModel = { id:0, IssueDate:new Date(), IssueOrder:"", cus_id: 0, cus_name:"",  due_date:"",  cus_contact : "", cus_tel: "",  cus_address: "", com_contact: "", com_tel:"",  com_address: "", sub_total : 0, total : 0, vat:0,sub_alphabet:"",  sign_name:"",  sign_date: new Date(), status:"1" };
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"cus_id" : 0,
							"pro_id" : 0,
							"IssueOrder": "" 
						}; 
						
		$scope.TempSearchCustomerIndex  = {}; 
		$scope.TempSearchProjectIndex  = {}; 
		
		$scope.LoadSearch();
    }
	
	$scope.setCompanyData = function(){
		  
		$scope.CreateModel.com_contact = $scope.CompanyModel.name;
		$scope.CreateModel.com_tel = $scope.CompanyModel.tel;
		$scope.CreateModel.com_email = $scope.CompanyModel.email;
		$scope.CreateModel.com_address = $scope.CompanyModel.address1 + "\n" +  $scope.CompanyModel.address2 + "\n" +  $scope.CompanyModel.address3;
	}
	
	
	$scope.reload = function (){
		
		if("undefined" == typeof $scope.TempSearchCustomerIndex.selected){
			$scope.modelSearch.cus_id = 0;
		}else{
			$scope.modelSearch.cus_id = $scope.TempSearchCustomerIndex.selected.id;
		}
		
		if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
			$scope.modelSearch.pro_id = 0;
		}else{
			$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		}
		 
		
		quatationApiService.listquatation($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
				$scope.modelDeviceList = result.message; 
			}else{
				baseService.showMessage(result.message);
			}
		})
	}
	
 

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () { 
		
		$scope.resetModel();
		$scope.addQuatationDetail();
		
		$scope.listStatus.push({status:1, name:"กำลังดำเนินการ" });
		$scope.listStatus.push({status:2, name:"ยกเลิก" });
		$scope.listStatus.push({status:3, name:"อนุมัติ" });
		
		customerApiService.getComboBox(null, function(results){
			
			var result = results.data;
			
			if(true == result.status){
				 $scope.listCustomer =  result.message
				 
				 if($scope.listCustomer.length > 0){
					$scope.TempCustomerIndex.selected =  $scope.listCustomer[0];
					$scope.loadFromCustomer();
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		
		systemApiService.getCompany(null, function (result) {  
			if(result.status === true){ 
				$scope.CompanyModel = result.message[0];
				$scope.setCompanyData();
			}else{
				baseService.showMessage(result.message);
			}
		});
		
		projectApiService.getComboBox(null, function(results){
			
			var result = results.data;
			
			if(true == result.status){
				 $scope.listProject =  result.message
				 
				 if($scope.listProject.length > 0){
					$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		
		
		 
		$scope.resetSearch(); 
        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });
		
		//$scope.reload();
 
    }
	
	$scope.loadStatusDlg = function(item){
		 //console.log(item);
		 $scope.TempStatusId  = item.id;
		 
		 $scope.listStatus.forEach(function (entry) {
			 console.log(entry.status, item.status);
            if (item.status == entry.status)
                $scope.TempStatusIndex.selected = entry;
         });
		
		 $("#statusConfirm").modal('show');
		
	}
	
	$scope.UpdateStatus = function(){
		var jsonData = {}; 
		jsonData.id = $scope.TempStatusId;
		jsonData.status =  $scope.TempStatusIndex.selected.status;
		//console.log(jsonData);
		
		$("#statusConfirm").modal('hide');
		
		quatationApiService.updatestatusquatation(jsonData, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });
	}
	
	$scope.loadFromCustomer = function(){
		  
		$scope.CreateModel.cus_contact = $scope.TempCustomerIndex.selected.contact;
		$scope.CreateModel.cus_tel = $scope.TempCustomerIndex.selected.tel;
		$scope.CreateModel.cus_address = $scope.TempCustomerIndex.selected.address1 + "\n" +  $scope.TempCustomerIndex.selected.address2 + "\n" +  $scope.TempCustomerIndex.selected.address3;
		 
	}
	
	
	 $scope.onDeleteTagClick = function(item){
		quatationApiService.deletequatation({id:item.id}, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.IssueOrder == ""){
			$(".CreateModel_IssueOrder").show();
			bResult = false;
		}
		
		if($scope.CreateModel.sub_alphabet == ""){
			$(".CreateModel_sub_alphabet").show();
			bResult = false;
		}
		
		if($scope.CreateModel.sub_total == "" && $scope.CreateModel.sub_total != 0){
			$(".CreateModel_sub_total").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.total == "" && $scope.CreateModel.total != 0){
			$(".CreateModel_total").show();
			bResult = false;
		}
		
		if($scope.CreateModel.sign_name == ""){
			$(".CreateModel_sign_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.cus_contact == ""){
			$(".CreateModel_cus_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.cus_tel == ""){
			$(".CreateModel_cus_tel").show();
			bResult = false;
		}
		
		if($scope.CreateModel.cus_address == ""){
			$(".CreateModel_cus_address").show();
			bResult = false;
		}
		
		if($scope.CreateModel.com_contact == ""){
			$(".CreateModel_com_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.com_tel == ""){
			$(".CreateModel_com_tel").show();
			bResult = false;
		}
		
		if($scope.CreateModel.com_address == ""){
			$(".CreateModel_com_address").show();
			bResult = false;
		}
		
		return bResult;
	}
	
	$scope.addQuatationDetail = function(){ 
		 
		$scope.QuatationDetailModel  = [];
		
		for(var i = 1; i < 26;i++){
			$scope.QuatationDetailModel.push(
				{ id:0, qt_id:0, line_no: i, item_no:"", qty:"", unit:"", price:0, amount:0, item_desc :""}
			)
		}
		
	}
 
	$scope.onSaveTagClick = function () {
      
	  var bValid = $scope.validatecheck();
	  
	  $scope.CreateModel.detail = $scope.QuatationDetailModel;
	  $scope.CreateModel.cus_id = $scope.TempCustomerIndex.selected.id;
	  $scope.CreateModel.cus_name = $scope.TempCustomerIndex.selected.name;
	  $scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
	  
	  if(true == bValid){
		quatationApiService.savequatation($scope.CreateModel, function (result) { 
			if(result.status == true){ 
				$scope.ShowDevice();
			}else{
				baseService.showMessage(result.message);
			} 
		}); 
	  }
    } 

	$scope.printPDF = function(item){ 
		var urlName = get_base_url("quatation/printPDF?id=" + item.id);
		window.open(urlName);
	}
}]); 