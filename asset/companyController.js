myApp.controller('companyController', ['$scope', '$filter',  'baseService', 'companyApiService', 'projectApiService',  function ($scope, $filter,  baseService, companyApiService, projectApiService ) {
    $scope.CreateModel = {};
	$scope.modelSearch = {};
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('company_id');
    $scope.SortOrder = baseService.setSortOrder('desc');
	
	$scope.listCompany = [];
	$scope.TempCompanyIndex = {};
	$scope.TempSearchCompanyIndex = {};
	
    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

     $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
        $scope.resetModel (); 
		 
		 
		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) {  
        $scope.AddNewDevice(); 
		$scope.loadEditData(item);              
    }
	
    $scope.loadEditData = function (item) { 
		$scope.CreateModel = angular.copy(item);
		// console.log($scope.CreateModel);
		// $scope.CreateModel.id =  item.id;
		// $scope.CreateModel.input_date =  item.input_date;
		// $scope.CreateModel.item =  item.item;
		// $scope.CreateModel.price =  parseFloat(item.price);
		// $scope.CreateModel.type =  item.type;
		// $scope.CreateModel.note =  item.note;
		
		// if($scope.listProject.length > 0){ 
		// 	$scope.listProject.forEach(function (entry, index) {
		// 		if (item.pro_id === entry.id)
		// 			$scope.TempProjectIndex.selected = entry;
		// 	});
		// }
	}
	
	$scope.resetModel = function () {

        $scope.CreateModel = { company_id:0, company_code:"",company_name:""};
		 
		//  if($scope.listProject.length > 0){
		// 	$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
		//  }
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"company_code": "",
							"company_name": ""
						};
						
		// $scope.TempSearchProjectIndex = {};
		// $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
	$scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		
		// projectApiService.getComboBox(null, function(results){
			
		// 	var result = results.data;
			
		// 	if(true == result.status){
		// 		 $scope.listProject =  result.message
				 
		// 		 if($scope.listProject.length > 0){
		// 			$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
		// 		 }
				 
		// 	}else{
		// 		baseService.showMessage(result.message);
		// 	}
		// });
		

        // $scope.listPageSize.forEach(function (entry, index) {
        //     if (0 === index)
        //         $scope.TempPageSize.selected = entry;
        // });
		
		$scope.reload();
    }
	
	$scope.reload = function (){
		 
		// if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
		// 	$scope.modelSearch.pro_id = 0;
		// }else{
		// 	$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		// }
		
		companyApiService.listcompany($scope.modelSearch, function (results) {  
            // console.log(results);
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;// 
				// console.log($scope.totalPage);
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.listCompany = result.message;
                // console.log($scope.listCompany);
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		
		
		companyApiService.deletecompany({company_id:item.company_id}, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}
	
	$scope.addCommas = function (nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '.00';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
	
	$scope.showPrice = function(item){
		var price = 0.00;
		
		if(item.type == 2){
			price =  (item.price * -1);
		}else{
			price =  item.price;
		} 
		
		return $scope.addCommas(price);
	}
	
	$scope.showColor = function(item){
		 
		if(item.type == 2){
			return "text-danger";
		}else{
			return  "text-primary";
		} 
		 
	}
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		
		  
		if($scope.CreateModel.item == ""){
			$(".CreateModel_item").show();
			bResult = false;
		}
		
		// if($scope.CreateModel.byear == "" && $scope.CreateModel.byear < 2000){
			// $(".CreateModel_byear").show();
			// bResult = false;
		// }
		
		// if(($scope.CreateModel.budget == "" && $scope.CreateModel.budget != 0) || $scope.CreateModel.budget == null){
			// $(".CreateModel_budget").show();
			// bResult = false;
		// }
	 
		if($scope.CreateModel.price <= 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
		
		$scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
		$scope.CreateModel.pro_name = $scope.TempProjectIndex.selected.name;
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
    //    var bValid = $scope.validatecheck();
	  
	// 	if(true == bValid){
            companyApiService.savecompany($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });
		// }
    }


}]); 