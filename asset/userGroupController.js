myApp.controller('userGroupController', ['$scope', '$filter',  'baseService', 'userGroupApiService','userApiService', function ($scope, $filter,  baseService, userGroupApiService, userApiService ) {
    $scope.CreateModel = {};
	$scope.modelSearch = {};
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('id');
    $scope.SortOrder = baseService.setSortOrder('asc');
	$scope.role_id=[];
	$scope.listUserGroup = [];
	$scope.listCompany =[];
	$scope.listUser =[];
	$scope.TempUserIndex = {};
	// $scope.listCostcenter =[];
	$scope.TempUserGroupIndex = {};
	// $scope.TempCostcenterIndex ={};
	$scope.TempSearchUserGroupIndex = {};
	$scope.TempCompanyIndex ={};
    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

     $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function (item) {
        $scope.resetModel (); 
		if(item == undefined){
			// console.log("testadd");
			userGroupApiService.loadUserGroupid(null, function (results) {  
				
				$scope.CreateModel.id = results[0]['id'];
				// console.log($scope.CreateModel);
			})
		}
		 
		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
		$(".addDevice").show();
		$(".assign").show(); 
		$(".dropbox").hide(); 
    }
	
	$scope.onEditTagClick = function (item) {  
		
		$scope.AddNewDevice(item); 
		$scope.loadEditData(item);    

	}

    $scope.loadEditData = function (item) { 
		$(".assign").show(); 
		$(".dropbox").hide(); 
		$scope.CreateModel.id =  item.id;
		$scope.CreateModel.group_name =  item.group_name;
		$scope.CreateModel.level_approve =  item.level_approve;
		$scope.CreateModel.approve_min =  item.approve_min;
		$scope.CreateModel.approve_max =  item.approve_max;
		
		// $scope.CreateModel.assign_approver = 1;
		// if($scope.CreateModel.assign_approver === 1){
		// 	$(".dropbox").show(); 
		// }else{
		// 	$(".dropbox").hide(); 
		// }
		// if($scope.listCompany.length > 0){ 
		// 	$scope.listCompany.forEach(function (entry, index) {
				// console.log(item.level_approve);
		// 		if (item.company_id === entry.company_id)
		// 			$scope.TempCompanyIndex.selected = entry;
		userApiService.getApproverCombobox($scope.CreateModel, function(result){
			// var result = results.data;
			
			if(true == result.status){
						 $scope.listUser =  result.message;
						//  console.log( $scope.listUser[0]);
						 userGroupApiService.getApprover($scope.CreateModel, function(results){
							// console.log($scope.listUser);
							// console.log(results.message[0]['assign_approver']);
							if(results.message[0]['assign_approver'] === '1'){
								$scope.CreateModel.assign_approver=1;
								$(".dropbox").show();
							}else{
								$scope.CreateModel.assign_approver=0;
							}
							// console.log(results.message[0]['approver_user_id']);
							$scope.listUser.forEach(function(entry, index){
								// console.log(entry.user_id);
								
								
								if(results.message[0]['approver_user_id'] === entry.user_id){
									$scope.TempUserIndex.selected = entry; 
								}
								// else{
								// 	$scope.TempUserIndex.selected = ""; 
								// }
							});
						//  if( $scope.listUser.length > 0){
							// console.log(results.message[0]['approver_user_id']);
							
						// 	$scope.TempUserIndex.selected =  $scope.listUser[0]; 
						//  }
						});
			}else{
				baseService.showMessage(result.message);
				}
		});
		
			// var result = results.data;
			// console.log(results);
			// console.log(results.data);
		// 	if(true == result.status){
		// 		 $scope.listCostcenter =  result.message
				
		// 		 console.log($scope.listCostcenter); 
		// 		 if($scope.listCostcenter.length > 0){
		// 			$scope.TempCostcenterIndex.selected =  $scope.listCostcenter[0]; 
		// 		 }
				 
		// 	}else{
		// 		baseService.showMessage(result.message);
		// 	}
					
		// 	});
		// }
		// if($scope.listCostcenter.length > 0){ 
		// 	$scope.listCostcenter.forEach(function (entry, index) {
		// 		// console.log(item.cost_center_code);
		// 		if (item.cost_center_code === entry.cost_code)
				
		// 		$scope.TempCostcenterIndex.selected = entry;
				
		// 	});
		// }
		// if($scope.listProject.length > 0){ 
		// 	$scope.listProject.forEach(function (entry, index) {
		// 		if (item.pro_id === entry.id)
		// 			$scope.TempProjectIndex.selected = entry;
		// 	});
		// }
	}
	
	$scope.resetModel = function () {

        $scope.CreateModel = { id:0,  group_name : "", level_approve: "", approve_min:"", approve_max:"" };
		 
	// 	 if($scope.listCompany.length > 0){
	// 		$scope.TempCompanyIndex.selected =  $scope.listCompany[0]; 
	// 	 }
	// 	 if($scope.listCostcenter.length > 0){
	// 		$scope.TempCostcenterIndex.selected =  $scope.listCostcenter[0]; 
	// 	 }
    }
	
	$scope.assign_approver_check = function () {
		if( $scope.CreateModel.assign_approver===1){
			$(".dropbox").show(); 
		}else{
			$scope.CreateModel.assign_approver
			$(".dropbox").hide(); 
		}
        
    }
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"id": "",
							"group_name":"",
							
						};
						
		
		// $scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
	$scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		
		// userApiService.getApproverCombobox($scope.CreateModel, function(result){
		// 	// var result = results.data;
			
		// 	if(true == result.status){
		// 				 $scope.listUser =  result.message;
		// 				 console.log( $scope.listUser[0]);
				
		// 				 if( $scope.listUser.length > 0){
							
		// 					$scope.TempUserIndex.selected =  $scope.listUser[0]; 
		// 				 }
						 
		// 	}else{
		// 		baseService.showMessage(result.message);
		// 		}
		// });
        
	
		// costcenterApiService.getComboBox(null, function(result){
		
			// var result = results.data;
			// console.log(results);
			// console.log(results.data);
		// 	if(true == result.status){
		// 		 $scope.listCostcenter =  result.message
				
		// 		 console.log($scope.listCostcenter); 
		// 		 if($scope.listCostcenter.length > 0){
		// 			$scope.TempCostcenterIndex.selected =  $scope.listCostcenter[0]; 
		// 		 }
				 
		// 	}else{
		// 		baseService.showMessage(result.message);
		// 	}
		
		
			
    
			 
		

		// companyApiService.getComboBox(null, function(result){
		
		// 	// var result = results.data;
			
		// 	if(true == result.status){
		// 		 $scope.listCompany =  result.message
				
		// 		 console.log($scope.listCompany); 
		// 		 if($scope.listCompany.length > 0){
		// 			$scope.TempCompanyIndex.selected =  $scope.listCompany[0]; 
		// 		 }
				 
		// 	}else{
		// 		baseService.showMessage(result.message);
		// 	}

		// });
		

        // $scope.listPageSize.forEach(function (entry, index) {
        //     if (0 === index)
        //         $scope.TempPageSize.selected = entry;
        // });
		
		$scope.reload();
    }
	
	$scope.reload = function (){
		 
		// if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
		// 	$scope.modelSearch.pro_id = 0;
		// }else{
		// 	$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		// }
		// console.log($scope.modelSearch);
		userGroupApiService.listUserGroup($scope.modelSearch, function (results) {  
            
        var result= results.data; 
        console.log(result.message);
			if(result.status === true){  
				
				$scope.totalPage = result.totalPage;// console.log($scope.toTalPage);
				$scope.listPageIndex = baseService.getListPage(result.totalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.listUserGroup = result.message;
        //         // console.log($scope.listUser);
			}else{
				
			}
        })
        
	}
 
    $scope.onDeleteTagClick = function(item){
		
		
		userGroupApiService.deleteUserGroup(item, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}


	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		
		  
		// if($scope.CreateModel.item == ""){
		// 	$(".require").show();
		// 	bResult = false;
		// }
		
		if($scope.CreateModel.group_name == ""){
			$(".CreateModel_group_name").show();
			bResult = false;
        }
        if($scope.CreateModel.level_approve == ""){
			$(".CreateModel_level_approve").show();
			bResult = false;
		}
		if($scope.CreateModel.approve_min == ""){
			$(".CreateModel_approve_min ").show();
			bResult = false;
        }
        if($scope.CreateModel.approve_max == ""){
			$(".CreateModel_approve_max ").show();
			bResult = false;
		}
		
		// if(($scope.CreateModel.budget == "" && $scope.CreateModel.budget != 0) || $scope.CreateModel.budget == null){
			// $(".CreateModel_budget").show();
			// bResult = false;
		// }
	 
		// if($scope.CreateModel.price <= 0){
		// 	$(".CreateModel_price").show();
		// 	bResult = false;
		// }
		
		// $scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
		// $scope.CreateModel.pro_name = $scope.TempProjectIndex.selected.name;
		
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {
	   var bValid = $scope.validatecheck();
	//    console.log($scope.TempUserIndex.selected.user_id);
	
	   if($scope.CreateModel.assign_approver===1){
		   
			$scope.CreateModel.approver_user_id = $scope.TempUserIndex.selected.user_id;
		   
		   
		}else{
			$scope.CreateModel.approver_user_id =0;
		}
	//    $scope.CreateModel.cost_code = $scope.TempCostcenterIndex.selected.cost_code;
   console.log($scope.TempUserIndex.selected.user_id);
		if(true == bValid){
			userGroupApiService.saveUserGroup($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });
		}
            
	}
		
    

    
}]); 