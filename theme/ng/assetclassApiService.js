'use strict';
myApp.factory('assetclassApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Assetclass/");
    return {
          
        //AssetClass 
        listAssetClass: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getAssetClassList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        loadClassid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxClassid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveAssetClass: function (model, onComplete) {
            baseService.postObject(servicebase + 'addAssetClass', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteAssetClass: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteAssetClass', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getComboAssetClassList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);