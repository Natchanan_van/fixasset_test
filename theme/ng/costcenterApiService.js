'use strict';
myApp.factory('costcenterApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("CostCenter/");
    return {
          
        //CostCenter 
        listCostcenter: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCostcenterList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        loadCostid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxCostid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveCostCenter: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCostCenter', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteCostCenter: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCostCenter', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCostcenterComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);