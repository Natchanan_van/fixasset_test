'use strict';
myApp.factory('fixassetApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("FixAsset/");
    return {
          
        //FixAsset 
	    listFixasset: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getFixAssetModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

		getFixasset: function (model, onComplete) {
            baseService.postObject(servicebase + 'getFixAssetModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },

       
        saveFixasset: function (model, onComplete) {
            baseService.postObject(servicebase + 'addFixAsset', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteFixAsset: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteFixAsset', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		// getComboBox: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getCustomerComboList', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        
    };
}]);