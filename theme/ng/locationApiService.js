'use strict';
myApp.factory('locationApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Location/");
    return {
          
        //Location 
	    listLocation: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getLocationModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    loadLocationid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxLocationid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveLocation: function (model, onComplete) {
            baseService.postObject(servicebase + 'addLocation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteLocation: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteLocation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getLocationComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);