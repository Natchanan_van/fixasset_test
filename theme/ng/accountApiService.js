'use strict';
myApp.factory('accountApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Account/");
    return {
          
        //Account 
        listAccount: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getAccountModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    loadAccountid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxAccountid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveAccount: function (model, onComplete) {
            baseService.postObject(servicebase + 'addAccount', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteAccount: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteAccount', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getAccountComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);