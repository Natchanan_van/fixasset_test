'use strict';
myApp.factory('userGroupApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("UserGroup/");
    return {
          
        //CostCenter 
	   
		listUserGroup: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getuserGroupModelList',model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveUserGroup: function (model, onComplete) {
            baseService.postObject(servicebase + 'saveUserGroup', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteUserGroup: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteUserGroup', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        loadUserGroupid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxUserGroupid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getUserGroupComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getApprover: function (model, onComplete) {
            baseService.postObject(servicebase + 'getApprover', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);