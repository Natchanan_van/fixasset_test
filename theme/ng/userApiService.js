'use strict';
myApp.factory('userApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("User/");
    return {
          
        //CostCenter 
	   
		listUser: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getuserModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveUser: function (model, onComplete) {
            baseService.postObject(servicebase + 'saveUser', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteUser: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteUser', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        loadUserid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxUserid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxlv1: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserlv1ComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxlv2: function (model, onComplete) {
            baseService.postObject(servicebase + 'getuserlv2ComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getApproverCombobox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getApprover', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);