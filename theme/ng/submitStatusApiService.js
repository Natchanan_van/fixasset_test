'use strict';
myApp.factory('submitStatusApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("SubmitStatus/");
    return {
          
        //SubmitStatus 
  
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getSubmitStatusComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);