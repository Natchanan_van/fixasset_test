'use strict';
myApp.factory('reportApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("report/");
    return {
          
        //report 
	    listreport: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getReportModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
		//report total
	    listtotalreport: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getTotalReportModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
        
    };
}]);