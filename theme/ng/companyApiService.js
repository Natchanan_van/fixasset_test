'use strict';
myApp.factory('companyApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Company/");
    return {
          
        //CostCenter 
	   
		listcompany: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCompanyComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savecompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCompany', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletecompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCompany', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCompanyComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);