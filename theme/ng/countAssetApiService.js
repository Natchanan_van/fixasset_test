'use strict';
myApp.factory('countAssetApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("countAsset/");
    return {
          
        //CountAsset 
        listCountAsset: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCountAssetModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        listCountAssetDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCountAssetDetailList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    loadAccountid: function (model, onComplete) {
            baseService.postObject(servicebase + 'loadMaxAccountid', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveAccount: function (model, onComplete) {
            baseService.postObject(servicebase + 'addAccount', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteAccount: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteAccount', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getAccountComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);