<?php
	class Request_model extends CI_Model{

		//view for api service

		public function insert_request_header ($data){
			$this->db->insert('t_request_header', $data);
			return true;
		
		}

		public function insert_request_detail ($data){
			$this->db->insert('t_request_detail', $data);
			return true;
		}

		public function get_duplicate_asset_in_request($reqest_no, $it_asset){

			$sql = " select * from t_request_detail
			where request_no = '$reqest_no' and asset_id = '$it_asset'
			";
			$query = $this->db->query($sql);

			if ($query->num_rows() == 0){
				return false;
			}
			else{
				return $result = $query->result_array();
			}
			
		}

		public function get_request_by_user($user_id){

			$sql = " select *,date(request_date) as request_date_format
            from v_request_header
			where user_request = '$user_id'
			order by request_date desc
			";

            $query = $this->db->query($sql);

			return $result = $query->result_array();
			
		}

		public function get_asset_list_by_req_num($req_num){

			$sql = " select * , IFNULL(asset_image,'asset_demo.png') as asset_image2
            from v_request_detail
			where req_num = '$req_num'
			order by request_detail_id desc
			";

            $query = $this->db->query($sql);

			return $result = $query->result_array();
			
		}

		
		public function delete_asset_from_list($request_detail_id){

			$sql = " delete
            from t_request_detail
			where request_detail_id = '$request_detail_id'
			";

            $query = $this->db->query($sql);
			$result = true;

			return $result ;
			
		}

		public function submit_request($req_number){

			$sql = "update t_request_header set submit_status = '2'
			where req_number = '$req_number'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}

		public function delete_request_detail($req_number){

			$sql = "delete from t_request_detail
			where request_no = '$req_number'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}

		public function delete_request_header($req_number){

			$sql = "delete from t_request_header
			where req_number = '$req_number'
			and submit_status = '1'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}

		public function insert_approve_list($req_number){

			$sql_check_dup = " select * from t_request_approve_list
			where req_number = '$req_number'";
			$query = $this->db->query($sql_check_dup);
			

			if ($query->num_rows() == 0){

				$sql = "insert into t_request_approve_list
				(req_number,req_type,user_request_id,role_requestor,role_approver,user_approve_id,level_approve,approve_status)
				SELECT req_number ,req_type,user_request,role,id,user_approve_id,level_approve,'1'
				from v_flow_approve as t1 
				where flow_flag = 1 and req_number = '$req_number'
				order by level_approve asc
				";
				$query = $this->db->query($sql);
				
				$result = true;
				return $result ;
				
			}else{
				$result = false;
				return $result ;
			}

			//  insert to approve list
			
			

			//
		}

		public function get_request_by_uniqid($ref_uniqid){
			$query = $this->db->get_where('t_request_header', array('ref_uniqid' => $ref_uniqid));
			return $result = $query->result_array();
			
		}

		// end of view for api serice


		public function insert_approve_level ($req_number){

		
			$sql = " insert into t_approve_level(req_number,submit_status,role_id,approve_min,approve_max,book_val,it_approve)
            select req_number,submit_status,id,approve_min,approve_max,book_val,it_approve
            from v_role_approve
            where req_number = '$req_number'
            and role_approve = '1'
			";

            $query = $this->db->query($sql);
			return true ;
		}

		public function update_user_approver_level($req_number){
			$sql = "
				update t_approve_level t1 
				left outer join v_role_approve_user t2 on t1.req_number = t2.req_number and t1.role_id = t2.role_id
				set t1.user_approver = t2.user_approver
				where t1.req_number = '$req_number'
			";

			$query = $this->db->query($sql);
			return ;
		}

		public function update_request_status($req_no,$from_approver,$approve_status,$approve_date){
		
			$sql = "update t_request_approve_list
					set approve_status = '$approve_status', approve_date = '$approve_date'
					where req_number = '$req_no' and user_approve_id = '$from_approver' and approve_status = '1' ";

			$query = $this->db->query($sql);
			return true;
		}

		public function update_request_status_header($req_no,$approve_status){
		
			$sql = "update t_request_header
					set request_status = '$approve_status'
					where req_number = '$req_no' and request_status = '1' ";

			$query = $this->db->query($sql);
			return true;
		}

		public function get_req_number_from_trans_id ($req_number){
			$sql = " select distinct req_number
            from v_role_approve_user
            where trans_id = '$req_number'";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		public function get_request_header_description ($req_no){
			$sql = "select t1.req_number, t1.req_type, t1.to_costcenter, t1.submit_status, t1.request_date, t1.user_request
			,t2.type_name , t3.username , t3.firstname , t3.lastname , t3.email , t1.price
			from t_request_header t1

			left outer join t_request_type t2 on t1.req_type = t2.type_id
			left outer join ci_users t3 on t1.user_request = t3.id

            where req_number = '$req_no'";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		
		public function request_total_book_value ($req_no){
			$sql = "select request_no, count(*) as qty_asset, sum(net_value) as total_book_val from t_request_detail
			where request_no = '$req_no'
			group by request_no
			";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		public function current_approver($req_no){

			$sql = " select * from v_request_current_approve
			where req_number = '$req_no' 
			";
			$query = $this->db->query($sql);

			if ($query->num_rows() == 0){
				return false;
			}else{
				return $result = $query->result_array();
			}			
		}


		public function get_approve_list($req_no){

			$sql = " select * from v_request_approve_list
			where req_number = '$req_no' 
			";
			$query = $this->db->query($sql);

			if ($query->num_rows() == 0){
				return false;
			}else{
				return $result = $query->result_array();
			}			
		}

		public function get_admin_purchasing_email(){
			$sql = "select * from ci_users where role = '13'";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		public function get_account_manager_email(){
			$sql = "select * from ci_users where role = '10'";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		public function get_head_of_finance_email(){
			$sql = "select * from ci_users where role = '11'";

            $query = $this->db->query($sql);
			return $result = $query->result_array();
			
		}

		// function for history log
		public function insert_history_log($req_no){
			$sql = "insert into t_history_log
			(req_number,req_type,from_costcenter,to_costcenter,asset_id,request_date,approved_date,request_by,net_value)
			SELECT req_number,req_type,from_costcenter,to_costcenter,asset_id,request_date,approved_date,request_by,net_value
			from v_for_insert_history_log 
			where req_number = '$req_no'
			";
			$query = $this->db->query($sql);
			
			$result = true;
			return $result ;
		}

		public function update_asset_type_transfer($req_no){
			
			$sql = "
			update t_fa as t1
			inner join ( 	select req_number,req_type,from_costcenter,to_costcenter,asset_id,request_date,approved_date,request_by,net_value
							from v_for_insert_history_log where req_number = '$req_no') as t2 on t1.asset_id=t2.asset_id
			set t1.costcenter = t2.to_costcenter";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
		}

		public function update_asset_type_disposal($req_no){
			$sql = "
			update t_fa as t1
			inner join ( 	select req_number , asset_id
							from v_for_insert_history_log where req_number = '$req_no') as t2 on t1.asset_id=t2.asset_id
			set t1.disposal_status = '1'";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
		}

		public function update_request_selling_price($req_no,$approve_status,$price){
			$sql = "
			update t_request_header as t1
			set t1.price = '$price' 
			where req_number = '$req_no'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
		}

	}

?>

