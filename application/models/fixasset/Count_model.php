<?php
	class Count_model extends CI_Model{

		//view for api service

		public function insert_count_header ($data){
			$this->db->insert('t_count_header', $data);
			return true;
		
		}

		public function get_count_by_uniqid($ref_uniqid){
			$query = $this->db->get_where('t_count_header', array('ref_uniqid' => $ref_uniqid));
			return $result = $query->result_array();
			
		}

		public function get_duplicate_asset_in_count($count_no, $asset_id){

			$sql = " select * from t_count_detail
			where count_no = '$count_no' and asset_id = '$asset_id'
			";
			$query = $this->db->query($sql);

			if ($query->num_rows() == 0){
				return false;
			}
			else{
				return $result = $query->result_array();
			}
			
		}


		public function insert_count_detail ($data){
			$this->db->insert('t_count_detail', $data);
			return true;
		}

		public function get_count_by_user($user_id){

			$sql = " select *,date(count_no_date) as count_date
            from v_count_header
			where user_create_count_no = '$user_id'
			order by count_no_date desc
			";

            $query = $this->db->query($sql);

			return $result = $query->result_array();
			
		}

		public function get_asset_list_by_count_no($count_no){

			// $sql = " select * , IFNULL(asset_image,'asset_demo.png') as asset_image2
			$sql = " select *
            from v_count_detail
			where count_no = '$count_no'
			order by count_detail_id desc
			";

            $query = $this->db->query($sql);

			return $result = $query->result_array();
			
		}

		public function delete_asset_from_count_list($count_detail_id){

			$sql = " delete
            from t_count_detail
			where count_detail_id = '$count_detail_id'
			";

            $query = $this->db->query($sql);
			$result = true;

			return $result ;
			
		}

		public function delete_count_detail($count_no){

			$sql = "delete from t_count_detail
			where count_no = '$count_no'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}

		public function delete_count_header($count_no){

			$sql = "delete from t_count_header
			where count_no = '$count_no'
			and submit_status = '1'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}

		public function submit_count($count_no){

			$sql = "update t_count_header set submit_status = '2'
			where count_no = '$count_no' and submit_status = '1'
			";
            $query = $this->db->query($sql);
			
			$result = true;

			return $result ;
			
		}


	}

?>

