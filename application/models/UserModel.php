<?php
  
class UserModel extends CI_Model {
	
    private $tbl_name = 'user';
	private $id = 'user_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getUserNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	public function checkUserID($user_id){
		$sql =  "SELECT *FROM ".$this->tbl_name." WHERE user_id = ".$user_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	public function checkUserPass($user_id,$pass){
		$sql =  "SELECT password FROM ".$this->tbl_name." WHERE user_id = ".$user_id." and password = '".$pass."'";

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	public function maxUserid(){
      
		$sql =  "SELECT MAX(user_id)+1 AS user_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	 
    public function validate($username, $password){
        // grab user input
       // $username = $this->security->xss_clean($this->input->post('uname'));
        //$password = $this->security->xss_clean($this->input->post('pswsss'));
        
        // Prep the query
        $this->db->where('user', $username);
        $this->db->where('password', md5($password));
        
        // Run the query
        $query = $this->db->get( $this->tbl_name );
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
			//print_r($row);
            $data = array(
                    'id' => $row->id,
                    'user' => $row->user, 
                    'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }else{
			//echo "count ". $query->num_rows(). "no found ".  md5($password). $username;
		}
        // If the previous process did not validate
        // then return false.
        return false;
    }
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getUserNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getUserModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['user_name']) && $dataModel['user_name'] != ""){
		 	$sql .= " and user like '%".$this->db->escape_str( $dataModel['user_name'])."%' ";
		}
	
	   if(isset($dataModel['cost_center_code']) && $dataModel['cost_center_code'] != ""){
			$sql .= " and cost_center_code like '%".$this->db->escape_str( $dataModel['cost_center_code'])."%' ";
	   }
	

		
		// if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	// $sql .= " and num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		// }
		
		// if(isset($dataModel['com_name']) && $dataModel['com_name'] != ""){
		 	// $sql .= " and com_name like '%".$this->db->escape_str( $dataModel['com_name'])."%' ";
		// }
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		// print_r($query);
		return  $query->num_rows() ;
	}
	
	public function getUserNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name." LEFT JOIN t_company ON "
				. $this->tbl_name.".company_id = t_company.company_id  
				LEFT JOIN t_user_group ON ". $this->tbl_name.".role = t_user_group.id  WHERE "
				. $this->tbl_name.".delete_flag = 0  ";// AND User_name LIKE ?";
		$sql =  $this->getSearchQuery($sql, $dataModel);
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteUsername($id){
		$result = false;
		try{
			$query = $this->getUserNameById($id);
			$modelData;			
			// foreach ($query->result() as $row)
			// {
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			// }
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getUserlv1ComboList($cost_code){
		
		$sql = "SELECT user_id, user, cost_center_code
		FROM ". $this->tbl_name . " WHERE role = 2 AND cost_center_code = ".$cost_code;
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getUserlv2ComboList($cost_code){
		
		$sql = "SELECT user_id, user, cost_center_code
		FROM ". $this->tbl_name . " WHERE role = 3 AND cost_center_code = ".$cost_code;
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getApproverList($role_id){
		
		$sql = "SELECT * FROM ". $this->tbl_name 
		. " WHERE role =".$role_id;
		$query = $this->db->query($sql);
		return  $query->result_array();
		// return $sql;
	}
	// Listall
	// public function getUserList($dataModel,$limit = 10 ,$offset=0,$order ='' ,$direction ="asc"){
		
	// 	$sql = "SELECT * FROM ". $this->tbl_name ." WHERE delete_flag = 0  ";
	// 	$sql =  $this->getSearchQuery($sql, $dataModel);
	// 	// print_r($sql);
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }
}
?>