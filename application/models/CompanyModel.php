<?php
  
class CompanyModel extends CI_Model {
	
    private $tbl_name = 't_company';
	private $id = 'company_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getCompanyNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getCompanyNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Company_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getCompanyModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Company_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getCompanyNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name;//. " WHERE Company_delete_flag = 0 AND Company_name LIKE ?";
		  
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['Company_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteCompany($id){
		$result = false;
		try{
			$query = $this->getCompanyNameById($id);
			$modelData;	
			$modelData =array('delete_flag'=>1);
			/*$query = $this->getCompanyNameById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user_name'),
					'Company_delete_flag' => 1 //$row->Company_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
			
        	return $this->db->update($this->tbl_name, $modelData);*/
			//return $this->update($id, $modelData);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getCompanyComboList($dataModel ,$limit = 10 , $offset = 0 , $Order = '' , $direction ='asc' ){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE delete_flag = 0";
		$sql =  $this->getSearchQuery($sql, $dataModel);
		// if($order != ""){
		// 	$sql .= " ORDER BY ".$order."".$direction;
		// }else{
		// 	$sql .= " ORDER BY ".$this->company_id."".$direction;
		// }
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['company_code']) && $dataModel['company_code'] != ""){
		 	$sql .= " and company_code like '%".$this->db->escape_str( $dataModel['company_code'])."%' ";
		}
		if(isset($dataModel['company_name']) && $dataModel['company_name'] != ""){
			$sql .= " and company_name like '%".$this->db->escape_str( $dataModel['company_name'])."%' ";
	   }
		return $sql;
	}
	public function getTotal($dataModel){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
}
?>