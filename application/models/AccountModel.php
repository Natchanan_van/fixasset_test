<?php
  
class AccountModel extends CI_Model {
	
    private $tbl_name = 't_account';
	private $id = 'account_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getAccountById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function maxAccountid(){
      
		$sql =  "SELECT MAX(account_id)+1 AS account_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function checkAccountID($class_id){
		$sql =  "SELECT *	FROM ".$this->tbl_name." WHERE account_id = ".$class_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}

	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['code']) && $dataModel['code'] != ""){
		//  	$sql .= " and code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		// }
		
		if(isset($dataModel['account_code']) && $dataModel['account_code'] != ""){
		 	$sql .= " and account_code like '%".$this->db->escape_str( $dataModel['account_code'])."%' ";
		}
		
		if(isset($dataModel['account_name']) && $dataModel['account_name'] != ""){
		 	$sql .= " and account_name like '%".$this->db->escape_str( $dataModel['account_name'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getAccountModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE delete_flag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
			
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteAccount($account_id){
		$result = false;
		try{
			$query = $this->getAccountById($account_id);
			$modelData;			
				
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			
			
			$this->db->where($this->id, $account_id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getAccountComboList(){
		
		$sql = "SELECT account_id,account_code,account_name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>