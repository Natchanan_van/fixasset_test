<?php
  
class CostCenterModel extends CI_Model {
	
    private $tbl_name = 't_cost_center';
	private $id = 'cost_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getCostCenterById($cost_id){
		$this->db->where($this->id, $cost_id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 

		
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getCostcenterModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
       
        
        $sql = "SELECT cost.*, com.company_code , com.company_name 
				FROM t_cost_center cost 
				INNER JOIN t_company com ON cost.company_id = com.company_id
				WHERE cost.delete_flag = 0  ";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);		
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		$sql .= " LIMIT ".$offset.", ".$limit;
		$query = $this->db->query($sql);
		
	
		// return $dataModel;
		return $query->result_array();
		// return $sql;
	}
	
	public function maxCostid(){
      
		$sql =  "SELECT MAX(cost_id)+1 AS cost_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function checkCostID($cost_id){
		$sql =  "SELECT *	FROM ".$this->tbl_name." WHERE cost_id = ".$cost_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['company_id']) && $dataModel['company_id'] != ""){
		 	$sql .= " and cost.company_id = ".$this->db->escape_str( $dataModel['company_id']);
		}
		
		if(isset($dataModel['cost_code']) && $dataModel['cost_code'] != ""){
		 	$sql .= " and cost_code like '%".$this->db->escape_str( $dataModel['cost_code'])."%' ";
		}
		
		if(isset($dataModel['cost_description']) && $dataModel['cost_description'] != ""){
		 	$sql .= " and cost_description like '%".$this->db->escape_str( $dataModel['cost_description'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){

	
		
		$sql = "SELECT cost.*, com.company_code , com.company_name 
				FROM t_cost_center cost 
				INNER JOIN t_company com ON cost.company_id = com.company_id
				WHERE cost.delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
 
	public function deleteCostCenter($cost_id){
		$result = false;
		try{
			$query = $this->getCostCenterById($cost_id);
			$modelData;			
			
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			
			
			$this->db->where($this->id, $cost_id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getCostcenterComboList(){
		
		$sql = "SELECT cost_id,cost_description,cost_code FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>