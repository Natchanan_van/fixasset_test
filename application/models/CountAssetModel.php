<?php
  
class CountAssetModel extends CI_Model {
	
    private $tbl_name = 'v_count_header';
	private $id = 'count_no';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getCountAssetById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function maxCountAssetid(){
      
		$sql =  "SELECT MAX(CountAsset_id)+1 AS CountAsset_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function checkCountAssetID($class_id){
		$sql =  "SELECT *	FROM ".$this->tbl_name." WHERE CountAsset_id = ".$class_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}

	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['code']) && $dataModel['code'] != ""){
		//  	$sql .= " and code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		// }
		
		// if(isset($dataModel['company_id']) && $dataModel['company_id'] != ""){
		//  	$sql .= " where chead.company_id = ".$this->db->escape_str( $dataModel['company_id']);
		// }
		
		if(isset($dataModel['cost_id']) && $dataModel['cost_id'] != ""){
		 	$sql .= " and cost.cost_id = ".$this->db->escape_str( $dataModel['cost_id']);
		}

		if(isset($dataModel['countasset_start']) && $dataModel['countasset_end'] != ""){
			$sql .= " and chead.count_date = between '".$this->db->escape_str( $dataModel['countasset_start'])."' AND '".$this->db->escape_str( $dataModel['countasset_end'])."'";
	   }
						
	   if(isset($dataModel['submit_status']) && $dataModel['submit_status'] != ""){
		$sql .= " and chead.submit_status = ".$this->db->escape_str( $dataModel['submit_status']);
   }

		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT chead.*,cost.* 
				FROM `v_count_header` chead INNER JOIN t_cost_center cost 
				ON chead.costcenter_code = cost.cost_code"	;			
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getCountAssetModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$dataPost = json_decode( $this->input->raw_input_stream , true);

		$start = isset($dataPost['countasset_start'])?$dataPost['countasset_start']: "";
		$end = isset($dataPost['countasset_end'])?$dataPost['countasset_end']: "";

		$sql = "SELECT chead.*,cost.* 
                FROM `v_count_header` chead INNER JOIN t_cost_center cost 
				ON chead.costcenter_code = cost.cost_code";
			
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
			
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		
		$query = $this->db->query($sql);
		return  $query->result_array();
    }
    
    public function getCountAssetDetailList($count_no){
		
		$sql = "SELECT cdetail.* , asset.*
				FROM v_count_detail cdetail INNER JOIN v_asset_detail asset 
				ON cdetail.asset_id = asset.asset_id
				WHERE cdetail.count_no = ".$count_no; 
		
		// $sql =  $this->getSearchQuery($sql, $dataModel);	
		
			
		// if($order != ""){
		// 	$sql .= " ORDER BY ".$order." ".$direction;
		// }else{
		// 	$sql .= " ORDER BY ".$this->id." ".$direction;
		// }
		
		// $sql .= " LIMIT $offset, $limit";
		
		// return $sql;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteCountAsset($CountAsset_id){
		$result = false;
		try{
			$query = $this->getCountAssetById($CountAsset_id);
			$modelData;			
				
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			
			
			$this->db->where($this->id, $CountAsset_id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getCountAssetComboList(){
		
		$sql = "SELECT CountAsset_id,CountAsset_code,CountAsset_name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>