<?php
  
class SubmitStatusModel extends CI_Model {
	
    private $tbl_name = 't_submit_status';
	private $id = 'submit_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getAccountById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	

	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['code']) && $dataModel['code'] != ""){
		//  	$sql .= " and code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		// }
		
		if(isset($dataModel['account_code']) && $dataModel['account_code'] != ""){
		 	$sql .= " and account_code like '%".$this->db->escape_str( $dataModel['account_code'])."%' ";
		}
		
		if(isset($dataModel['account_name']) && $dataModel['account_name'] != ""){
		 	$sql .= " and account_name like '%".$this->db->escape_str( $dataModel['account_name'])."%' ";
		}
		
		return $sql;
	}
	

	
	public function getSubmitStatusComboList(){
		
		$sql = "SELECT * FROM ". $this->tbl_name;
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>