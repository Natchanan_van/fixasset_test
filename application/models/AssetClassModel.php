<?php
  
class AssetClassModel extends CI_Model {
	
    private $tbl_name = 't_class';
	private $id = 'class_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getAssetClassById($cost_id){
		$this->db->where($this->id, $cost_id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 

		
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getAssetClassModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
       
        
        $sql = "SELECT * FROM ". $this->tbl_name. " WHERE delete_flag = 0";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);		
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		$sql .= " LIMIT ".$offset.", ".$limit;
		$query = $this->db->query($sql);
		
	
		// return $dataModel;
		return $query->result_array();
		// return $sql;
	}
	
	public function maxClassid(){
      
		$sql =  "SELECT MAX(class_id)+1 AS class_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	public function checkClassID($class_id){
		$sql =  "SELECT *	FROM ".$this->tbl_name." WHERE class_id = ".$class_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
	
		
		if(isset($dataModel['class_code']) && $dataModel['class_code'] != ""){
		 	$sql .= " and class_code like '%".$this->db->escape_str( $dataModel['class_code'])."%' ";
		}
		
		if(isset($dataModel['class_description']) && $dataModel['class_description'] != ""){
		 	$sql .= " and class_description like '%".$this->db->escape_str( $dataModel['class_description'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){

	
		    
        $sql = "SELECT * FROM ". $this->tbl_name. " WHERE delete_flag = 0";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
 
	public function deleteAssetClass($class_id){
		$result = false;
		try{
			$query = $this->getAssetClassById($class_id);
			$modelData;			
			
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
			
			
			$this->db->where($this->id, $class_id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	// public function getAssetClassComboList(){
		
	// 	$sql = "SELECT cost_id,cost_description FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
	// 	$query = $this->db->query($sql);
	// 	return  $query->result_array();
	// }
	
}
?>