<?php
  
class LocationModel extends CI_Model {
	
    private $tbl_name = 't_location';
	private $id = 'location_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getLocationById($location_id){
		$this->db->where($this->id, $location_id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	

	
	public function getLocationModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
       
        
        $sql = "SELECT * FROM ". $this->tbl_name. " WHERE delete_flag = 0";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);		
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		$sql .= " LIMIT ".$offset.", ".$limit;
		// $sql .= " LIMIT 0, ".$limit;
		$query = $this->db->query($sql);
		
	
		// return $dataModel;
		return $query->result_array();
		// return $sql;
	}

	public function checkLocationID($location_id){
		$sql =  "SELECT *	FROM ".$this->tbl_name." WHERE location_id = ".$location_id;

		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}

	public function maxLocationid(){
      
		$sql =  "SELECT MAX(location_id)+1 AS location_id FROM ".$this->tbl_name;
		
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['location_cat']) && $dataModel['location_cat'] != ""){
		 	$sql .= " and location_cat like '%".$this->db->escape_str( $dataModel['location_cat'])."%' ";
		}
		
		// if(isset($dataModel['name']) && $dataModel['name'] != ""){
		//  	$sql .= " and name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
		// }
		
		// if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		//  	$sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		// }
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	

 
	public function deleteLocation($location_id){
		$result = false;
		try{
			$query = $this->getLocationById($location_id);
			$modelData;			
			
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user'),
					'delete_flag' => 1 //$row->Project_delete_flag 
				); 
		
			
			$this->db->where($this->id, $location_id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getLocationComboList(){
		
		$sql = "SELECT location_id,location_cat FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>