<!DOCTYPE html>
<html>
<head> 
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo base_url('/theme/image/favicon.png');?>" />
<title>Fix Asset Management</title>
<style>
body {font-family: Arial, Helvetica, sans-serif;  }
form {border: 3px solid #f1f1f1; width: 300px;
 margin: auto; padding-top:20px;margin-top: 50px; }

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 60%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
</head>
<body  background="theme/image/background1.jpg">
 

<form action="<?php echo base_url('/Login');?>" method="POST">
  <div class="imgcontainer">
    <img src="<?php echo base_url('/theme/image/Dhlecom.jpg');?>" alt="Avatar" class="img-fluid" height="150" width="300">
  </div>

  <div class="container">
	<h2 style="text-align:center" ></h2>

    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="pswsss" required>
    
	<label><?php echo $msgError ; ?></label>
    <button type="submit">Login</button>
   <?php /* <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>*/ ?>
  </div> 
</form>

</body>
</html>