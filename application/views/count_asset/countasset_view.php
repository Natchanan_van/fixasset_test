<script src="<?php echo base_url('asset/countAssetController.js');?>"></script>
 <div  ng-controller="countAssetController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('CountAsset');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('CountAsset');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							
							<div class="col-lg-6 col-md-6 col-xs-12">

								<label><?php echo $this->lang->line('CostCenter');?></label>
									<ui-select ng-model="TempSearchCostCenterIndex.selected"   theme="selectize" >
										<ui-select-match>{{$select.selected.cost_description}}</ui-select-match>

										<ui-select-choices repeat="pIndex in listCostCenter | filter: $select.search">
											<span ng-bind-html="pIndex.cost_description  | highlight: $select.search"></span>
										</ui-select-choices>
									</ui-select>
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('SubmitStatus');?></label>
								<ui-select ng-model="TempSearchSubmitStatusIndex.selected"   theme="selectize" >
										<ui-select-match>{{$select.selected.submit_description}}</ui-select-match>

										<ui-select-choices repeat="pIndex in listSubmitStatus | filter: $select.search">
											<span ng-bind-html="pIndex.submit_description  | highlight: $select.search"></span>
										</ui-select-choices>
									</ui-select>
								<p class="help-block"></p> 
							</div> 
							
						</div>
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('CountDateStart');?></label>
								<input class="form-control" ng-model="modelSearch.countdate_start" maxlength="80" data-date-format="dd-MMM-yyyy"  bs-datepicker>
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								
								<label><?php echo $this->lang->line('CountDateEnd');?></label>{{modelSearch.countdate_end}}
								<input class="form-control" ng-model="modelSearch.countdate_end" maxlength="80" data-date-format="dd-MMM-yyyy"  bs-datepicker>
								<p class="help-block"></p>
							</div> 
							
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4><?php echo $this->lang->line('CountNo');?> : <label  ng-bind="CreateModel.count_no" ></label></h4>
							<h4><?php echo $this->lang->line('CostCode');?> : <label  ng-bind="CreateModel.cost_code" ></label></h4>
							<h4><?php echo $this->lang->line('CostDescription');?> : <label  ng-bind="CreateModel.cost_description" ></label></h4>
							<h4><?php echo $this->lang->line('CountDate');?> : <label  ng-bind="CreateModel.count_date | date :'dd MMM yyyy' " ></label></h4>
							<h4><?php echo $this->lang->line('UserCreate');?> : <label  ng-bind="CreateModel.user" ></label></h4>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
                                    <div class="table-responsive">
								    <table class="table table-striped">
                                        <thead>
                                            <tr> 
                                                <!-- <th class="sorting_asc" ng-click="sort($event)"  sort="count_no"><?php echo $this->lang->line('No');?></th> -->
                                                <th class="sorting" ng-click="sort($event)"  sort="costcenter_code"><?php echo $this->lang->line('AssetNo');?></th>
                                                <th class="sorting" ng-click="sort($event)"   sort="asset_description"><?php echo $this->lang->line('AssetDescription');?></th> 
                                                <th class="sorting" ng-click="sort($event)"  sort="account_code"><?php echo $this->lang->line('AccountCode');?></th> 
                                                <th class="sorting" ng-click="sort($event)"  sort="account_name"><?php echo $this->lang->line('AccountName');?></th>
												<th class="sorting" ng-click="sort($event)"  sort="cap_date"><?php echo $this->lang->line('CapDate');?></th> 
												<th class="sorting" ng-click="sort($event)"  sort="acquisition"><?php echo $this->lang->line('Acquisition');?></th>
												<th class="sorting" ng-click="sort($event)"  sort="total"><?php echo $this->lang->line('Total');?></th>
												<th class="sorting" ng-click="sort($event)"  sort="book_value"><?php echo $this->lang->line('BookVal');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="detail in countAssetDetail">
                                                <!-- <td ng-bind="detail.count_detail_id"  width=5%></td>  -->
                                                <td ng-bind="detail.asset_no" width=10%></td>
                                                <td ng-bind="detail.asset_description" width=25%></td>
                                                <td ng-bind="detail.account_code"  width=10%></td>
                                                <td ng-bind="detail.account_name"  width=10%></td>
												<td ng-bind="detail.cap_date | date :'dd MMM yyyy'" width=10%></td>
												<td ng-bind="detail.acquisition"  width=10%></td>
												<td ng-bind="detail.total"  width=10%></td>
												<td ng-bind="detail.book_value"  width=10%></td>
												 
                                            </tr> 
                                        </tbody>
								    </table>
							</div>
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<!-- <button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button> -->
											<button class="btn btn-primary"  ng-click="" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('ExportFile');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfCountAsset');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<!-- <button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>  -->
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
							<button class="btn btn-primary"  ng-click="" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('ExportFile');?></span></button>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th class="sorting_asc" ng-click="sort($event)"  sort="count_no"><?php echo $this->lang->line('CountNo');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="costcenter_code"><?php echo $this->lang->line('CostCode');?></th>
                                            <th class="sorting" ng-click="sort($event)"  sort="cost_description"><?php echo $this->lang->line('CostDescription');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="count_date"><?php echo $this->lang->line('CountDate');?></th> 
											<th class="sorting" ng-click="sort($event)"  sort="submit_description"><?php echo $this->lang->line('SubmitStatus');?></th> 
                                            <th class="sorting" ng-click="sort($event)"  sort="user_create_count_no"><?php echo $this->lang->line('UserCreate');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.count_no"></td> 
                                            <td ng-bind="item.costcenter_code"></td>
                                            <td ng-bind="item.cost_description"></td>
											<td ng-bind="item.count_date"></td>
                                            <td ng-bind="item.submit_description"></td>
                                            <td ng-bind="item.user"></td>
                                            <td>
												
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('ViewDatail');?></span></button>
												<!-- <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button> -->
												
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- จำนวนหน้า page -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>