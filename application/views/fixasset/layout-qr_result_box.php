<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DHL ECommerce Fix Asset QR Code</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

</head>
<body>
            
           <div class="container" style="text-align:left;  font-size: large" >
                <div class=col-md-12>
                    <div class="box box-solid">
                        <div class="box-body">
                                <img src="<?= base_url('assets/images/dhl-logo-full.png')?>" alt="" style="width:100%;">
                                <!-- <h1 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;"> -->
                                <h1 style="background-color:#C41515; color:white; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                    Fix Asset Information
                                </h1>
                                <div class="row">
                                <hr>
                                    Asset Code : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['asset_no']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Barcode : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['qrcode']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Asset Description : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['asset_description']; ?>
                                                <?php endforeach; ?>
                                </div>
                                
                                <div class="row">
                                <hr>
                                    Company : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['company_code']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Class : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['class_code']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Account Code : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['account_code']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Account Name : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['account_name']; ?>
                                                <?php endforeach; ?>
                                </div>
                                

                                <div class="row">
                                <hr>
                                    Cost center code : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['cost_code']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Cost center name : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['cost_description']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Capital date : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['cap_date']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Acquisition : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['acquisition']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                Total Accumulate Dep : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['total']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <div class="row">
                                <hr>
                                    Net Book Value : <?php foreach($item_detail as $row): ?>
                                                        <?= $row['book_value']; ?>
                                                <?php endforeach; ?>
                                </div>

                                <br><br><br>


                                <!-- <div class="row">
                                <hr>
                                <img style="width:95%;" 
                                    src="<?php echo base_url('assets/uploads/asset_images/')?><?php foreach($item_detail as $row): ?><?= trim($row['asset_image']); ?><?php endforeach; ?>" alt="" 
                                </div>

                                <hr> -->


                            
                        </div>
                    </div>
                </div>
                

                <!-- <div class="box box-solid">
                        <div class="box-body">
                                
                                <div class="row">
                                    <form action="<?php echo base_url('itasset/change_asset/load_login'); ?>" method="post">
                                    <input class="form-control" type="hidden" name="qr_code_id" value="<?php foreach($item_detail as $row): ?><?= trim($row['qr_code_id']); ?><?php endforeach; ?>">
                                 
                                    <button type="submit" class="btn btn-primary btn-lg btn-block btn-huge btn-block">Change</button>
                                    </form>
                                </div>
                                

                        </div>
                </div> -->

           </div>
         

</script> 

</body>
</html>