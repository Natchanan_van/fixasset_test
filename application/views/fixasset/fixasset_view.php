<script src="<?php echo base_url('asset/fixassetController.js');?>"></script>
 <div  ng-controller="fixassetController" ng-init="onInit()"  style="background-image:url(theme/image/background1.jpg)">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('FixAssetList');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('FixAssetList');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetNo');?></label>
								<input class="form-control" ng-model="modelSearch.asset_no" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12"> 
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyCode');?></label>
								<ui-select ng-model="TempSearchCompanyIndex.selected"   theme="selectize" >
											<ui-select-match>{{$select.selected.company_code}}</ui-select-match>

											<ui-select-choices repeat="pIndex in listCompany | filter: $select.search">
												<span ng-bind-html="pIndex.company_code  | highlight: $select.search"></span>
											</ui-select-choices>
								</ui-select>
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetDescription');?></label>
								<input class="form-control" ng-model="modelSearch.asset_description" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CostCenter');?></label>
									<ui-select ng-model="TempSearchCostCenterIndex.selected"   theme="selectize" >
										<ui-select-match>{{$select.selected.cost_description}}</ui-select-match>

										<ui-select-choices repeat="pIndex in listCostCenter | filter: $select.search">
											<span ng-bind-html="pIndex.cost_description  | highlight: $select.search"></span>
										</ui-select-choices>
									</ui-select>
								<p class="help-block"></p>
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Asset');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('QrCode');?></label> <br>
												<img src="assets/images/qrcode/{{CreateModel.qrcode}}.png" height="150" width="150">
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
												<ui-select ng-model="TempCompanyIndex.selected"   theme="selectize" >
													<ui-select-match>{{$select.selected.company_code}}</ui-select-match>

													<ui-select-choices repeat="pIndex in listCompany | filter: $select.search">
														<span ng-bind-html="pIndex.company_code  | highlight: $select.search"></span>
													</ui-select-choices>
												</ui-select>
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetID');?></label>
												<input class="form-control" ng-model="CreateModel.asset_id" maxlength="80" disabled>
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetNo');?></label>
												<input class="form-control" ng-model="CreateModel.asset_no" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetClass');?></label>
												<input class="form-control" ng-model="CreateModel.class_code" maxlength="80" >
												<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AssetDescription');?></label>
												<input class="form-control" ng-model="CreateModel.asset_description	"  maxlength="80">
												<p class="CreateModel_asset_description	 require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CostCenter');?></label>
												<ui-select ng-model="TempCostCenterIndex.selected"   theme="selectize" >
														<ui-select-match>{{$select.selected.cost_description}}</ui-select-match>

														<ui-select-choices repeat="pIndex in listCostCenter | filter: $select.search">
															<span ng-bind-html="pIndex.cost_description  | highlight: $select.search"></span>
														</ui-select-choices>
												</ui-select>
												<p class="CreateModel_cost_description require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Life');?></label>
												<input class="form-control" ng-model="CreateModel.life"  maxlength="50">
												<p class="CreateModel_tel require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CapDate');?></label>
												<input class="form-control" ng-model="CreateModel.cap_date"  maxlength="80"  data-date-format="dd-MMM-yyyy"  bs-datepicker>
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('ODep_start');?></label>
												<input class="form-control" ng-model="CreateModel.ODep_start"  maxlength="80"  data-date-format="dd-MMM-yyyy"  bs-datepicker>
												<p class="CreateModel_website require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div> 
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Acquisition');?></label>
													<input class="form-control" ng-model="CreateModel.acquisition"  maxlength="80">
													<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Total');?></label>
													<input class="form-control" ng-model="CreateModel.total"  maxlength="80">
													<p class="CreateModel_website require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('BookVal');?></label>
												<input class="form-control" ng-model="CreateModel.book_value"  maxlength="80">
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('AccountCode');?></label>
												<ui-select ng-model="TempAccountIndex.selected"   theme="selectize" >
														<ui-select-match>{{$select.selected.account_code}} - {{$select.selected.account_name}}</ui-select-match>

														<ui-select-choices repeat="pIndex in listAccount | filter: $select.search">
															<span ng-bind-html="(pIndex.account_code  | highlight: $select.search)+' - '+ (pIndex.account_name )"></span>
														</ui-select-choices>
												</ui-select>
												<p class="CreateModel_account_name require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('DocNo');?></label>
												<input class="form-control" ng-model="CreateModel.doc_no"  maxlength="80">
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Reference');?></label>
												<input class="form-control" ng-model="CreateModel.reference"  maxlength="80">
												<p class="CreateModel_website require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Location');?></label>
												<ui-select ng-model="TempLocationIndex.selected"   theme="selectize" >
														<ui-select-match>{{$select.selected.location_cat}}</ui-select-match>

														<ui-select-choices repeat="pIndex in listLocation | filter: $select.search">
															<span ng-bind-html="pIndex.location_cat  | highlight: $select.search"></span>
														</ui-select-choices>
												</ui-select>
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('QrCode');?></label>
												<input class="form-control" ng-model="CreateModel.qrcode"  maxlength="80" disabled>
												<p class="CreateModel_website require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfAsset');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<!-- <button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>  -->
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<!-- <th class="sorting_asc" ng-click="sort($event)"  sort="code"><?php echo $this->lang->line('AssetID');?></th> -->
											<th class="sorting" ng-click="sort($event)"  sort="name"><?php echo $this->lang->line('QrCode');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="name"><?php echo $this->lang->line('AssetNo');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="contact"><?php echo $this->lang->line('AssetDescription');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="company"><?php echo $this->lang->line('CompanyCode');?></th> 
											<th class="sorting_asc" ng-click="sort($event)"  sort="code"><?php echo $this->lang->line('CostCenter');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="name"><?php echo $this->lang->line('ODep_start');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="contact"><?php echo $this->lang->line('AssetClass');?></th>
											<th class="sorting_asc" ng-click="sort($event)"  sort="code"><?php echo $this->lang->line('CapDate');?></th>
											<!-- <th class="sorting" ng-click="sort($event)"  sort="name"><?php echo $this->lang->line('Acquisition');?></th> -->
											<th class="sorting" ng-click="sort($event)"  sort="contact"><?php echo $this->lang->line('BookVal');?></th>
											<?php /*<th><?php echo $this->lang->line('Tel');?></th>
											<th><?php echo $this->lang->line('taxid');?></th> */ ?>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
                                            <!-- <td ng-bind="item.asset_id"></td>  -->
                                            <td ng-bind="item.qrcode"></td>
                                            <td ng-bind="item.asset_no"></td>
                                            <td ng-bind="item.asset_description"  width=80></td>
											<td ng-bind="item.company_code"></td>
											<td ng-bind="item.cost_description" ></td> 
                                            <td ng-bind="item.ODep_start| date :'dd MMM yyyy'"></td>
                                            <td ng-bind="item.class_code"></td>
											<td ng-bind="item.cap_date  | date :'dd MMM yyyy' "></td> 
                                            <!-- <td ng-bind="item.acquisition"></td> -->
                                            <td ng-bind="item.book_value"></td>
                                           <?php /* <td ng-bind="item.tel"></td>
                                            <td ng-bind="item.taxid"></td> */ ?>
                                            <td>
												
													<a href="<?php echo base_url('FixAsset/printQrcode');?>/{{item.asset_id}}" target="_blank"><button><i class="glyphicon glyphicon-qrcode"></i> QR </button></a>
													<!-- <button ng-click="onPrintQR(item)" class="btn btn-warning waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-qrcode"></i> <span class="hidden-xs"><?php echo $this->lang->line('PrintQR');?></span></button> -->
													<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <?php echo $this->lang->line('Edit');?><span class="hidden-xs"></span></button> 
											
												<!-- <a href="<?php echo base_url('FixAsset/printQrcode');?>/{{item.asset_id}}" target="_blank"><button><i class="glyphicon glyphicon-qrcode"></i></button></a> -->
												<!-- <button ng-click="onPrintQR(item)" class="btn btn-warning waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-qrcode"></i> <span class="hidden-xs"><?php echo $this->lang->line('PrintQR');?></span></button> -->
												<!-- <button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"></span></button>  -->
												<!-- <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button> -->
												
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- จำนวนหน้า page -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>