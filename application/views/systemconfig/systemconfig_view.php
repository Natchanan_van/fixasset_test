<script src="<?php echo base_url('asset/systemConfigController.js');?>"></script>
 <div  ng-controller="systemConfigController" ng-init="onInit()">
  <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('Company');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Company');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			  
			<!-- / create room types  -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('CompanyDetail');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
												<input class="form-control" ng-model="CreateModel.name" maxlength="80" >
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact');?></label>
												<input class="form-control" ng-model="CreateModel.contact"  maxlength="80">
												<p class="help-block"></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('taxid');?></label>
												<input class="form-control" ng-model="CreateModel.taxid"  maxlength="60">
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Tel');?></label>
												<input class="form-control" ng-model="CreateModel.tel"  maxlength="50">
												<p class="help-block"></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email');?></label>
												<input class="form-control" ng-model="CreateModel.email"  maxlength="80">
												<p class="help-block"></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('website');?></label>
												<input class="form-control" ng-model="CreateModel.website"  maxlength="80">
												<p class="help-block"></p>
											</div> 
										</div> 
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address1');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address1"  maxlength="80"></textarea>
												<br/>
											</div> 
										</div>
										<?php /*<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address2');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address2"  maxlength="80"></textarea>
												<br/>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address3');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address3"  maxlength="80"></textarea>
												<br/>
											</div> 
										</div>
										*/ ?>
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<?php /*<button class="btn btn-danger"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>*/ ?>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
	</div>
</div>