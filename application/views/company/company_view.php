<script src="<?php echo base_url('asset/companyController.js');?>"></script>
<div  ng-controller="companyController" ng-init="onInit()">
 <div class="row">
	<ul class="navigator"> 
		<li class="nav_active"> <?php echo $this->lang->line('Company');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $this->lang->line('Company');?></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
  
<!-- / create room types  -->
<div class="row">
	<div class="col-lg-12">
	
		<!-- /List.row types-->
		<div class="row  SearchDevice" style="display:none;">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('Search');?>
					</div> 
					<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><?php echo $this->lang->line('Company');?></label>
							<input class="form-control" ng-model="modelSearch.company_code" maxlength="80">
							<label><?php echo $this->lang->line('company_name');?></label>
							<input class="form-control" ng-model="modelSearch.company_name" maxlength="80">
						</div> 
						<div class="col-lg-6 col-md-6 col-xs-12"> 
						</div> 
					</div> 
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
						<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
					</div>
					</div> 
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
	
		<div class="row addDevice" style="display:none;">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Income');?>
				</div>
				<div class="panel-body"> 
					
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12"> 
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('company_code');?></label>
										<input class ="form-control" ng-model="CreateModel.company_code" >
									</div> 
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('company_name');?></label>
										<input class="form-control" ng-model="CreateModel.company_name" >
										<p class="CreateModel_cus_id require text-danger"><?php echo $this->lang->line('require');?></p>
									</div>  
									<!-- <div class="col-lg-4 col-md-4 col-xs-12"> 
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('ProjectName');?></label>
										<ui-select ng-model="TempProjectIndex.selected" theme="selectize">
											<ui-select-match>{{$select.selected.name}}</ui-select-match>
											<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
												<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
											</ui-select-choices>
										</ui-select> 
										<p class="CreateModel_cus_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Date');?></label>
										<input class="form-control" ng-model="CreateModel.input_date" data-date-format="dd-MM-yyyy" bs-datepicker>
										<p class="CreateModel_input_date require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>  -->
								</div> 
								<!-- <div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Item');?></label>
										<input class="form-control" ng-model="CreateModel.item">
										<p class="CreateModel_item require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Price');?></label>
										<input class="form-control" ng-model="CreateModel.price" type="number">
										<p class="CreateModel_price require text-danger"><?php echo $this->lang->line('RequireMoreZero');?></p>
									</div>  
									<div class="col-lg-4 col-md-4 col-xs-12">
										<label><?php echo $this->lang->line('Note');?></label>
										<textarea class="form-control" ng-model="CreateModel.note" ></textarea>
										<p class="CreateModel_note require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>  
								</div>   -->
								<div class="form-group text-right">
									<br/><br/>
									<div class="col-lg-12 col-md-12 col-xs-12">
									<button class="btn btn-primary"  ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
									<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
									</div>
								</div>
							</div>
						</div>  
					
					<!-- /.row (nested) --> 
				</div> 
				<!-- /.panel-body -->
			</div>
		</div>
		
		
		<!-- /List.row types-->
		<div class="row DisplayDevice" >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('Company');?>
					</div> 
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
						<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="table-responsive"> 
							<table class="table table-striped">
								<thead>
									<tr>  
										<!-- <th><?php echo $this->lang->line('company_id');?></th> -->
										<th><?php echo $this->lang->line('company_code');?></th>
										<th><?php echo $this->lang->line('company_name');?></th>
										<!-- <th><?php echo $this->lang->line('create_user');?></th>  
										<th><?php echo $this->lang->line('create_date');?></th>
										<th><?php echo $this->lang->line('update_user');?></th>
										<th><?php echo $this->lang->line('update_date');?></th>
										<th><?php echo $this->lang->line('delete_flag');?></th> -->

									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in listCompany">
										<!-- <td ng-bind="item.company_id"></td>  -->
										<td ng-bind="item.company_code"></td>
										<td ng-bind="item.company_name"></td>
										<!-- <td ng-bind="item.create_user"></td>
										<td ng-bind="item.create_date"></td>
										<td ng-bind="item.update_user"></td>
										<td ng-bind="item.update_date"></td>
										<td ng-bind="item.delete_flag"></td> -->
										<!-- <td class="text-right" ng-class="showColor(item)" ng-bind="showPrice(item)"></td>  -->
										<td> 
											<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											 
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					
					  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.create room types -->


<!-- / create room Item  -->
 
</div>