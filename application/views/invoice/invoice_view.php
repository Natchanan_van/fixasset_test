<script src="<?php echo base_url('asset/invoiceController.js');?>"></script>
<div  ng-controller="invoiceController" ng-init="onInit()">
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('Invoice');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $this->lang->line('Invoice');?></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
  
<!-- / create room types  -->
<div class="row">
	<div class="col-lg-12">
	
		<!-- /List.row types-->
		<div class="row  SearchDevice" style="display:none;">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('Search');?>
					</div> 
					<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('NumNo');?></label>
							<input class="form-control" ng-model="modelSearch.IssueOrder" maxlength="80" >
						</div> 
						<div class="col-lg-6 col-md-6 col-xs-12"> 
						</div> 
					</div>
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><?php echo $this->lang->line('ProjectName');?></label>
							<ui-select ng-model="TempSearchProjectIndex.selected" theme="selectize">
								<ui-select-match>{{$select.selected.name}}</ui-select-match>
								<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
									<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
								</ui-select-choices>
							</ui-select> 
						</div> 
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><?php echo $this->lang->line('CustomerID');?></label>
							<ui-select ng-model="TempSearchCustomerIndex.selected" ng-change="loadFromCustomer()" theme="selectize">
								<ui-select-match>{{$select.selected.name}}</ui-select-match>
								<ui-select-choices repeat="pIndex in listCustomer | filter: $select.search">
									<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
								</ui-select-choices>
							</ui-select> 
						</div> 
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
						<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
						<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
					</div>
					</div> 
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
	
		<div class="row addDevice" style="display:none;">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Invoice');?>
				</div>
				<div class="panel-body"> 
					
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-6"> 
										&nbsp;
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Date');?></label>
										<input class="form-control" ng-model="CreateModel.IssueDate" data-date-format="dd-MM-yyyy" bs-datepicker>
										<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('NumNo');?></label>
										<input class="form-control" ng-model="CreateModel.IssueOrder">
										<p class="CreateModel_IssueOrder require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>
								<div class="form-group  col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-3 col-md-3 col-xs-3"> 
										&nbsp;
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('ProjectName');?></label>
										<ui-select ng-model="TempProjectIndex.selected" theme="selectize">
											<ui-select-match>{{$select.selected.name}}</ui-select-match>
											<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
												<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
											</ui-select-choices>
										</ui-select> 
										<p class="CreateModel_cus_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('CustomerID');?></label>
										<ui-select ng-model="TempCustomerIndex.selected" ng-change="loadFromCustomer()" theme="selectize">
											<ui-select-match>{{$select.selected.name}}</ui-select-match>
											<ui-select-choices repeat="pIndex in listCustomer | filter: $select.search">
												<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
											</ui-select-choices>
										</ui-select> 
										<p class="CreateModel_cus_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('DueDate');?></label>
										<input class="form-control" ng-model="CreateModel.due_date">
										<p class="help-block"></p>
									</div> 
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact');?></label>
										<input class="form-control" ng-model="CreateModel.cus_contact">
										<p class="CreateModel_cus_contact require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Tel');?></label>
										<input class="form-control" ng-model="CreateModel.cus_tel">
										<p class="CreateModel_cus_tel require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>  
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address');?></label>
										<textarea class="form-control" ng-model="CreateModel.cus_address" rows="3"></textarea>
										<p class="CreateModel_cus_address require text-danger"><?php echo $this->lang->line('Require');?></p>
										<br/>
									</div> 
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact'); echo $this->lang->line('Customer'); ?></label>
										<input class="form-control" ng-model="CreateModel.com_contact">
										<p class="CreateModel_com_contact require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Tel');?></label>
										<input class="form-control" ng-model="CreateModel.com_tel">
										<p class="CreateModel_com_tel require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>  
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email'); ?></label>
										<input class="form-control" ng-model="CreateModel.com_email">
										<p class="CreateModel_com_contact require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address');?></label>
										<textarea class="form-control"  ng-model="CreateModel.com_address" rows="3"></textarea>
										<br/>
									</div> 
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12 ">
									<button class="btn btn-primary" ng-click="AddNewLine()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12 ">
									<div class="col-lg-1 col-md-1 col-xs-1"> 
										<label><?php echo $this->lang->line('ITEM');?></label> 
									</div><div class="col-lg-4 col-md-4 col-xs-4">
										<label><?php echo $this->lang->line('DESCRIPTION');?></label> 
									</div><div class="col-lg-1 col-md-1 col-xs-1">
										<label><?php echo $this->lang->line('Quantity');?></label> 
									</div><div class="col-lg-1 col-md-1 col-xs-1">
										<label><?php echo $this->lang->line('Unit');?></label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<label><?php echo $this->lang->line('Price');?></label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<label><?php echo $this->lang->line('Amount');?></label> 
									</div> <div class="col-lg-1 col-md-1 col-xs-1">
										<label><?php echo $this->lang->line('Option');?></label> 
									</div> 
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12" ng-repeat="item in invoiceDetailModel">
									<div class="col-lg-1 col-md-1 col-xs-1" style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control" ng-model="item.item_no">
									</div><div class="col-lg-4 col-md-4 col-xs-4" style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control" ng-model="item.item_desc">
									</div><div class="col-lg-1 col-md-1 col-xs-1"  style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control"  ng-model="item.qty">
									</div><div class="col-lg-1 col-md-1 col-xs-1"  style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control" ng-model="item.unit">
									</div><div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control text-right" ng-model="item.price">
									</div><div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
										<input class="form-control text-right"  ng-model="item.amount" ng-change="loadAmount()"   >
									</div><div class="col-lg-1 col-md-1 col-xs-1"  style="padding-right: 2px;padding-left: 2px;">
										<button my-confirm-click="onDeleteLine(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
									</div> 
								</div>
								<div class="form-group col-lg-12" style="padding-top: 20px;"> 
									<div class="col-lg-4 col-md-4 col-xs-4 text-right">
										<label><?php echo $this->lang->line('Payment');?></label> 
									</div>
									<div class="col-lg-8 col-md-8 col-xs-8  " > 
										<textarea class="form-control" ng-model="CreateModel.payment" rows="3" ></textarea>
									</div> 
								</div>
								<div class="form-group col-lg-12"  > 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>&nbsp;</label> 
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4 text-right" >
										<label><?php echo $this->lang->line('SubTotal');?></label> 
									</div><div class="col-lg-4 col-md-4 col-xs-4">
										<input class="form-control text-right" ng-model="CreateModel.sub_total">
										<p class="CreateModel_sub_total  require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  > 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>&nbsp;</label> 
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4 text-right" >
										<label><?php echo $this->lang->line('VAT');?></label> 
									</div><div class="col-lg-4 col-md-4 col-xs-4">
										<input class="form-control  text-right " ng-model="CreateModel.vat">
										<p class="CreateModel_vat require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  > 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>&nbsp;</label> 
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4 text-right" >
										<label><?php echo $this->lang->line('Total');?></label> 
									</div><div class="col-lg-4 col-md-4 col-xs-4">
										<input class="form-control text-right" ng-model="CreateModel.total" >
										<p class="CreateModel_total  require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label><?php echo $this->lang->line('sub_alphabet');?></label> 
									</div> 
									<div class="col-lg-8 col-md-8 col-xs-8">
										<input class="form-control" ng-model="CreateModel.sub_alphabet">
										<p class="CreateModel_sub_alphabet require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-6 col-md-6 col-xs-6">
										&nbsp;
									</div>											
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label><?php echo $this->lang->line('SignName');?></label> 
										<input class="form-control" ng-model="CreateModel.sign_name">
										<p class="CreateModel_sign_name require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-6 col-md-6 col-xs-6">
										&nbsp;
									</div>											
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label><?php echo $this->lang->line('SignDate');?></label> 
										<input class="form-control" ng-model="CreateModel.sign_date" data-date-format="dd-MM-yyyy" bs-datepicker>
										<p class="CreateModel_sign_date require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
								</div>
								<div class="form-group text-right">
									<br/><br/>
									<div class="col-lg-12 col-md-12 col-xs-12">
									<button class="btn btn-primary"  ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
									<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
									</div>
								</div>
							</div>
						</div>  
					
					<!-- /.row (nested) --> 
				</div> 
				<!-- /.panel-body -->
			</div>
		</div>
		
		
		<!-- /List.row types-->
		<div class="row DisplayDevice" >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('ListOfInvoice');?>
					</div> 
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
						<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>  
										<th><?php echo $this->lang->line('Date');?></th>
										<th><?php echo $this->lang->line('NumNo');?></th>
										<th><?php echo $this->lang->line('Project');?></th>
										<th><?php echo $this->lang->line('Customer');?></th> 
										<th><?php echo $this->lang->line('Contact');?></th>
										<th><?php echo $this->lang->line('Tel');?></th>
										<th><?php echo $this->lang->line('Option');?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList">
										<td ng-bind="item.IssueDate"></td> 
										<td ng-bind="item.IssueOrder"></td>
										<td ng-bind="item.name"></td>
										<td ng-bind="item.cus_name"></td>
										<td ng-bind="item.cus_contact"></td>
										<td ng-bind="item.cus_tel"></td> 
										<td>
											<button ng-click="printPDF(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('Print');?></span></button>
											<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											 
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					
					  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.create room types -->


<!-- / create room Item  -->
 
</div>