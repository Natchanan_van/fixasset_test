   </div>
        <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="dialog-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"   data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myMessageModalLabel"><?php echo $this->lang->line('Confirmation');?></h4>
            </div>
            <div class="modal-body">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span> <?php echo $this->lang->line('DoYouWantToDelete');?></p>
            </div>
            <div class="modal-footer"> 
                <button type="button" id="btnNo" class="btn btn-danger"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> <span class="hidden-xs"><?php echo $this->lang->line('NoCancel');?></span></button>
                <button type="button" id="btnYes" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> <span class="hidden-xs"><?php echo $this->lang->line('Yes');?></span></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="myMessageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myMessageModalLabel"><?php echo $this->lang->line('Information');?></h4>
            </div>
            <div class="modal-body">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;" id="show_message"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button"   class="btn btn-danger"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> <span class="hidden-xs"><?php echo $this->lang->line('Close');?></span></button> 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

</body>

</html>