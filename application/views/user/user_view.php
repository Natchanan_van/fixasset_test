<script src="<?php echo base_url('asset/userController.js');?>"></script>
 <div  ng-controller="userController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('User');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('User');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
					
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CostCenterID');?></label>
								<input class="form-control" ng-model="modelSearch.cost_center_code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('User');?></label>
								<input class="form-control" ng-model="modelSearch.user_name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
						</div>
						
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('User');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('user_id');?></label>
												<input class="form-control" ng-model="CreateModel.user_id" maxlength="80" disabled>
												<p class="CreateModel_id require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email');?></label>
												<input class="form-control" ng-model="CreateModel.user_email" maxlength="80">
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('User');?></label>
                                                <input class="form-control" ng-model="CreateModel.user_name" maxlength="80">    
												<p class="CreateModel_user require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('password');?></label>
												<input type="password" class="form-control" ng-model="CreateModel.user_password" maxlength="80" >
												<p class="CreateModel_password require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
												<ui-select ng-model="TempCompanyIndex.selected" theme="selectize">
													<ui-select-match>{{$select.selected.company_name}}</ui-select-match>
													<ui-select-choices repeat="pIndex in listCompany | filter: $select.search">
													<span ng-bind-html="pIndex.company_name  | highlight: $select.search"></span>
												</ui-select-choices>
												
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CostCenter');?></label>
												<ui-select ng-model="TempCostcenterIndex.selected"  ng-click="test()" theme="selectize">
													<ui-select-match>{{$select.selected.cost_code}} - {{$select.selected.cost_description}}</ui-select-match>
													<ui-select-choices repeat="pIndex in listCostcenter | filter: $select.search">
													<span ng-bind-html="(pIndex.cost_code  | highlight: $select.search)+' - '+ (pIndex.cost_description )"></span>
												</ui-select-choices>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger" >*</span><?php echo $this->lang->line('role');?></label>
												<ui-select ng-model="TempUserGroupIndex.selected" theme="selectize">
													<ui-select-match>{{$select.selected.group_name}}</ui-select-match>
													<ui-select-choices repeat="pIndex in listUserGroup | filter: $select.search">
													<span ng-bind-html="pIndex.group_name  | highlight: $select.search"></span>
												</ui-select-choices>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
											</div> 
										</div>
										
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('User');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th  class="sorting_asc" ng-click="sort($event)"  sort="user_id"><?php echo $this->lang->line('user_id');?></th>
											<th class="sorting_asc" ng-click="sort($event)"  sort="company_id"><?php echo $this->lang->line('Company');?></th>
											<th class="sorting_asc" ng-click="sort($event)"  sort="cost_center_code"><?php echo $this->lang->line('CostCode');?></th> 
											<th class="sorting" ng-click="sort($event)"  sort="user"><?php echo $this->lang->line('User');?></th> 
											<th hidden class="sorting" ng-click="sort($event)"  sort="password"><?php echo $this->lang->line('password');?></th> 
											<th class="sorting" ng-click="sort($event)"  sort="email"><?php echo $this->lang->line('Email');?></th> 
											<th class="sorting" ng-click="sort($event)"  sort="role"><?php echo $this->lang->line('role');?></th> 
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in listUser">
                                            <td ng-bind="item.user_id"></td> 
                                            <td ng-bind="item.company_name"></td>
											<td ng-bind="item.cost_center_code"></td>
											<td ng-bind="item.user"></td>
											<td hidden ng-bind="item.password"></td>
											<td ng-bind="item.email"></td>
											<td ng-bind="item.group_name"></td>
                                            <td>
												
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
												
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- จำนวนหน้า page -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>