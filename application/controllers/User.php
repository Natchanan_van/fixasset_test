<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use  setasign\Fpdi;
class User extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('user/user_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function adduser() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('userModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0; 
			$data['book_no'] =  isset($dataPost['book_no'])?$dataPost['book_no']: "";
			$data['num_no'] = isset($dataPost['num_no'])?$dataPost['num_no']: "";
			$data['pay_name'] = isset($dataPost['pay_name'])?$dataPost['pay_name']: "";
			$data['pay_id'] =  isset($dataPost['pay_id'])?$dataPost['pay_id']: "";
			$data['pay_address'] = isset($dataPost['pay_address'])?$dataPost['pay_address']: "";
			$data['com_name'] =  isset($dataPost['com_name'])?$dataPost['com_name']: "";
			$data['com_id'] = isset($dataPost['com_id'])?$dataPost['com_id']: "";
			$data['com_address'] = isset($dataPost['com_address'])?$dataPost['com_address']: "";
			$data['salary_year'] = isset($dataPost['salary_year'])?$dataPost['salary_year']: "";
			$data['salary_pay'] = isset($dataPost['salary_pay'])?$dataPost['salary_pay']: "";
			
			$data['salary_wht'] =  isset($dataPost['salary_wht'])?$dataPost['salary_wht']: "";
			$data['fee_year'] = isset($dataPost['fee_year'])?$dataPost['fee_year']: "";
			$data['fee_pay'] = isset($dataPost['fee_pay'])?$dataPost['fee_pay']: "";
			$data['fee_wht'] =  isset($dataPost['fee_wht'])?$dataPost['fee_wht']: "";
			$data['copyfee_year'] = isset($dataPost['copyfee_year'])?$dataPost['copyfee_year']: "";
			$data['copyfee_pay'] =  isset($dataPost['copyfee_pay'])?$dataPost['copyfee_pay']: "";
			$data['copyfee_wht'] = isset($dataPost['copyfee_wht'])?$dataPost['copyfee_wht']: "";
			$data['interest_year'] = isset($dataPost['interest_year'])?$dataPost['interest_year']: "";
			$data['interest_pay'] = isset($dataPost['interest_pay'])?$dataPost['interest_pay']: ""; 
			 			 
			$data['interest_wht'] =  isset($dataPost['interest_wht'])?$dataPost['interest_wht']: "";
			$data['interest1_year'] = isset($dataPost['interest1_year'])?$dataPost['interest1_year']: "";
			$data['interest1_pay'] = isset($dataPost['interest1_pay'])?$dataPost['interest1_pay']: "";
			$data['interest1_wht'] =  isset($dataPost['interest1_wht'])?$dataPost['interest1_wht']: "";
			$data['interest2_year'] = isset($dataPost['interest2_year'])?$dataPost['interest2_year']: "";
			$data['interest2_pay'] =  isset($dataPost['interest2_pay'])?$dataPost['interest2_pay']: "";
			$data['interest2_wht'] = isset($dataPost['interest2_wht'])?$dataPost['interest2_wht']: ""; 
			
			$data['interest3_year'] =  isset($dataPost['interest3_year'])?$dataPost['interest3_year']: "";
			$data['interest3_pay'] = isset($dataPost['interest3_pay'])?$dataPost['interest3_pay']: "";
			$data['interest3_wht'] = isset($dataPost['interest3_wht'])?$dataPost['interest3_wht']: "";
			$data['interest4_year'] =  isset($dataPost['interest4_year'])?$dataPost['interest4_year']: "";
			$data['interest4_pay'] = isset($dataPost['interest4_pay'])?$dataPost['interest4_pay']: "";
			$data['interest4_wht'] =  isset($dataPost['interest4_wht'])?$dataPost['interest4_wht']: "";
			$data['interest21_year'] = isset($dataPost['interest21_year'])?$dataPost['interest21_year']: ""; 
			 
			$data['interest21_pay'] =  isset($dataPost['interest21_pay'])?$dataPost['interest21_pay']: "";
			$data['interest21_wht'] = isset($dataPost['interest21_wht'])?$dataPost['interest21_wht']: "";
			$data['interest22_year'] = isset($dataPost['interest22_year'])?$dataPost['interest22_year']: "";
			$data['interest22_pay'] =  isset($dataPost['interest22_pay'])?$dataPost['interest22_pay']: "";
			$data['interest22_wht'] = isset($dataPost['interest22_wht'])?$dataPost['interest22_wht']: "";
			$data['interest23_year'] =  isset($dataPost['interest23_year'])?$dataPost['interest23_year']: "";
			$data['interest23_pay'] = isset($dataPost['interest23_pay'])?$dataPost['interest23_pay']: ""; 
			
			$data['interest23_wht'] =  isset($dataPost['interest23_wht'])?$dataPost['interest23_wht']: "";
			$data['interest24_year'] = isset($dataPost['interest24_year'])?$dataPost['interest24_year']: "";
			$data['interest24_pay'] = isset($dataPost['interest24_pay'])?$dataPost['interest24_pay']: "";
			$data['interest24_wht'] =  isset($dataPost['interest24_wht'])?$dataPost['interest24_wht']: "";
			$data['interest25_year'] = isset($dataPost['interest25_year'])?$dataPost['interest25_year']: "";
			$data['interest25_pay'] =  isset($dataPost['interest25_pay'])?$dataPost['interest25_pay']: "";
			$data['interest25_wht'] = isset($dataPost['interest25_wht'])?$dataPost['interest25_wht']: ""; 
			
			$data['interest25_comment'] =  isset($dataPost['interest25_comment'])?$dataPost['interest25_comment']: "";
			$data['revenue_year'] = isset($dataPost['revenue_year'])?$dataPost['revenue_year']: "";
			$data['revenue_pay'] = isset($dataPost['revenue_pay'])?$dataPost['revenue_pay']: "";
			$data['revenue_wht'] =  isset($dataPost['revenue_wht'])?$dataPost['revenue_wht']: "";
			$data['revenue_comment'] = isset($dataPost['revenue_comment'])?$dataPost['revenue_comment']: "";
			$data['other_year'] =  isset($dataPost['other_year'])?$dataPost['other_year']: "";
			$data['other_pay'] = isset($dataPost['other_pay'])?$dataPost['other_pay']: ""; 
			$data['other_wht'] = isset($dataPost['other_wht'])?$dataPost['other_wht']: ""; 
			
			$data['other_comment'] =  isset($dataPost['other_comment'])?$dataPost['other_comment']: "";
			$data['total_pay'] = isset($dataPost['total_pay'])?$dataPost['total_pay']: "";
			$data['total_wht'] = isset($dataPost['total_wht'])?$dataPost['total_wht']: "";
			$data['total_alphabet'] =  isset($dataPost['total_alphabet'])?$dataPost['total_alphabet']: "";
			$data['onetime_user'] = isset($dataPost['onetime_user'])?$dataPost['onetime_user']:0; 
			$data['forever_user'] =  isset($dataPost['forever_user'])?$dataPost['forever_user']: 0; 
			$data['wht_user'] = isset($dataPost['wht_user'])?$dataPost['wht_user']: 0; 
			$data['other_user'] = isset($dataPost['other_user'])?$dataPost['other_user']: 0; 
			
			$data['other_user_comment'] =  isset($dataPost['other_user_comment'])?$dataPost['other_user_comment']: "";
			$data['sign_name'] = isset($dataPost['sign_name'])?$dataPost['sign_name']: "";
			$data['sign_date'] = isset($dataPost['sign_date'])?$dataPost['sign_date']: "";
			 
	  		// load model 
    		if ($data['id'] == 0) {  
    			$nResult = $this->userModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->userModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteuser(){
		try{
			$this->load->model('userModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['user_id'])?$dataPost['user_id']:0;// $this->input->post('ap_id');
			// print_r($id);
			$bResult = $this->userModel->deleteusername($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getuserModel(){
	 
		try{
			$this->load->model('userModel','',TRUE); 
			 
			$result['status'] = true;
			$result['message'] = $this->lang->line("savesuccess");
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getApprover(){
	 
		try{
			$this->load->model('userModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;
			 
			$result['status'] = true;
			$result['message'] = $this->userModel->getApproverList($id);

			// print_r($result);
			// echo $id;
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	
	public function getuserModelList(){
	 
		try{
			$this->load->model('UserModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->UserModel->getUserNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->UserModel->getTotal($dataModel);
			$result['totalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->userModel->getuserModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getUserlv1ComboList(){
	 
		try{ 
			$this->load->model('UserModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['cost_code'])?$dataPost['cost_code']:0;
			

			$result['status'] = true;
			$result['message'] = $this->UserModel->getUserlv1ComboList($id);
			// print_r($result);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getUserlv2ComboList(){
	 
		try{ 
			$this->load->model('UserModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['cost_code'])?$dataPost['cost_code']:0;
			$result['status'] = true;
			$result['message'] = $this->UserModel->getUserlv2ComboList($id);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function saveUser(){
	 
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('UserModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$data['user_id'] =  isset($dataPost['user_id'])?$dataPost['user_id']: 0; 
			$data['user'] =  isset($dataPost['user_name'])?$dataPost['user_name']: "";
			$data['password'] = isset($dataPost['user_password'])?$dataPost['user_password']: "";
			$data['email'] = isset($dataPost['user_email'])?$dataPost['user_email']: "";
			$data['cost_center_code'] = isset($dataPost['cost_code'])?$dataPost['cost_code']: "";
			$data['company_id'] = isset($dataPost['company_id'])?$dataPost['company_id']: "";
			$data['role'] = isset($dataPost['role'])?$dataPost['role']: "";
			// load model 
			$statusid = $this->UserModel->checkUserID($data['user_id']);
			$passchange = $this->UserModel->checkUserPass($data['user_id'],$data['password']);
			// //   echo $data['id'];
			//   echo $passchange;
			if($passchange == 0){
				
				$data['password'] = MD5($data['password']);
			}
    		if ($statusid == 0) {  
				$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
    			$nResult = $this->UserModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date']= date("Y-m-d H:i:s");
		      	$nResult = $this->UserModel->update($data['user_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    
	}
	public function loadMaxUserid(){

		$this->load->model('UserModel','',TRUE); 

		$user_id = $this->UserModel->maxUserid();

		echo json_encode($user_id, JSON_UNESCAPED_UNICODE);
	}

	public function printPDF(){
		//$this->pdi();
		
		define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		require(APPPATH .'fpdf/fpdf.php'); 
		require_once(APPPATH .'fpdi/autoload.php');
		 
		
		try {
			$this->load->model('userModel','',TRUE); 
			
			$id = isset($_GET['id'])?$_GET['id']: 0;
			   
			$query = $this->userModel->getuserNameById($id);			
			$userDatas = $query->result_array();
			$userData = $userDatas[0];
			
			//print_r($customerDetail);
			
			$filename = $_SERVER["DOCUMENT_ROOT"]. '/materia/user_Template.pdf';
			$pdf_name = $userData['num_no'].".pdf"; 
			$pdf = new Fpdi\FPDI('p','mm','A4');			
			$pdf -> AddPage(); 

			$pdf->setSourceFile($filename); 
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			// now write some text above the imported page
			
			$pdf->AddFont('AngsanaNew','','angsa.php');
			$pdf->AddFont('AngsanaNew','B','angsab.php');
			$pdf->AddFont('AngsanaNew','I','angsai.php');
			$pdf->SetFont('AngsanaNew','',12);
			
			//$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);
			
			  
			$sign_date = new DateTime($userData['sign_date']);
			
			$tab1 = 18;
			$tab2 = 20;
			$tab3 = 30;
			$tab3Ex = 70;
			$tab4 = 120;
			$tab5 = 158;
			$tab6 = 182;
			$taxid1 = 133;
			$taxid2 = 140;
			$taxid3 = 144;
			$taxid4 = 148;
			$taxid5 = 152;
			$taxid6 = 160;
			$taxid7 = 164;
			$taxid8 = 168;
			$taxid9 = 172;
			$taxid10 = 176;
			$taxid11 = 183;
			$taxid12 = 187;
			$taxid13 = 194;
			$tabEnd = 188;
			$tabSocial = 130;
			$tabFund = 180;
			$lineStart = 20;
			$lineBr = 6;
			
			//...
			//บรรทัด 1
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['book_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 2
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['num_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 3
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][12]));
			$lineStart += $lineBr;
			
			//บรรทัด 4
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 5
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_address']));
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 6
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][12]));
			$lineStart += $lineBr + 1;
			
			//บรรทัด 7
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 8
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_address']));
			$lineStart += (5*$lineBr) + 4;
			
			//บรรทัด 9
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['salary_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['salary_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['salary_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 10
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['fee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['fee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['fee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1;
			
			//บรรทัด 11
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1 ;
			
			//บรรทัด 12
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 13
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 14
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 15
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 16
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 17
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			
			//บรรทัด 18
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4 ;
			
			//บรรทัด 19
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_wht'],0,0,'R'); 
			$lineStart += $lineBr;
			
			//บรรทัด 20
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_wht'],0,0,'R'); 
			$lineStart += $lineBr - 1;
			
			//บรรทัด 21
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['interest25_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 22
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_wht'],0,0,'R'); 
			$lineStart += $lineBr + 1 ;
			
			//บรรทัด 23
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['other_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['other_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['other_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['other_wht'],0,0,'R'); 
			$lineStart += $lineBr + 2;
			
			//บรรทัด 24 
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['total_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['total_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			 
			//บรรทัด 25
			$pdf->SetXY($tab3Ex, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['total_alphabet'])); 
			$lineStart += $lineBr ;
			
			//บรรทัด 26
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , '0000')); 
			$pdf->SetXY($tabFund, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  '0000')); 
			$lineStart += (3*$lineBr) + 2;
			
			//บรรทัด 27
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['sign_name'])); 
			$lineStart += $lineBr - 1;
			
			$pdf->SetXY($tabSocial - 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('d'))); 
			$pdf->SetXY($tabSocial + 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('m'))); 
			$pdf->SetXY($tabSocial + 22, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('Y'))); 
			$lineStart += $lineBr + 2;
			
			// Output
			$pdf->Output($_SERVER["DOCUMENT_ROOT"].'/application/uploads/'. $pdf_name, 'I'); //D = download // I , F , S
			
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}
