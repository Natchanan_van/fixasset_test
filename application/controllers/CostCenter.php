<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CostCenter extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('master_data/costcenter_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addCostCenter() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('CostCenterModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['cost_id'] =  isset($dataPost['cost_id'])?$dataPost['cost_id']: 0;
			$data['company_id'] =  isset($dataPost['company_id'])?$dataPost['company_id']: 0;
			$data['cost_code'] =  isset($dataPost['cost_code'])?$dataPost['cost_code']: "";
			$data['cost_description'] = isset($dataPost['cost_description'])?$dataPost['cost_description']: "";
			$data['approver_level1'] = isset($dataPost['approver_level1'])?$dataPost['approver_level1']: "";
			$data['approver_level2'] = isset($dataPost['approver_level2'])?$dataPost['approver_level2']: "";
	
			
			//print_r($data);
		
			$statusid = $this->CostCenterModel->checkCostID($data['cost_id']);
	  		// load model 
    		if ($statusid == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0;
    			$nResult = $this->CostCenterModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->CostCenterModel->update($data['cost_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function loadMaxCostid(){

		$this->load->model('CostCenterModel','',TRUE); 

		$cost_id = $this->CostCenterModel->maxCostid();

		echo json_encode($cost_id, JSON_UNESCAPED_UNICODE);
	}
	
	public function deleteCostCenter(){
		try{
			$this->load->model('CostCenterModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$cost_id =  isset($dataPost['cost_id'])?$dataPost['cost_id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->CostCenterModel->deleteCostCenter($cost_id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getCostcenterList(){
	 
		try{
			$this->load->model('CostCenterModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CostCenterModel->getCostcenterModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CostCenterModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CustomerModel->getCustomerModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
    public function getCostcenterComboList(){
	 
		try{ 
			$this->load->model('CostCenterModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->CostCenterModel->getCostCenterComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);			
	}
}
