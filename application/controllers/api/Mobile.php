<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin:*');

class Mobile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->model('admin/Auth_model', 'Auth_model');
        // $this->load->model('admin/Service_center_model', 'Service_center_model');
        // $this->load->model('fa/Request_model', 'Request_model');
        // $this->load->model('fa/Fix_asset_model', 'Fix_asset_model');

        $this->load->model('Mobile/MobileUserModel','MobileUserModel');
        $this->load->model('fixasset/Count_model', 'Count_model');
        $this->load->model('fixasset/Fix_asset_model', 'Fix_asset_model');

    }

	public function index(){
		redirect(base_url('admin'));
    }

	public function testpost(){
		echo "Result-Post ";
		$username = $this->input->post('username');
        $password = $this->input->post('password');
		echo $username;
		echo " - ";
		echo $password;
		//var_dump($this->input->raw_input_stream);
	}
    
	public function testget(){
		echo "Result-Get ";
		$username = $this->input->get('username');
        $password = $this->input->get('password');
		echo $username;
		echo " - ";
		echo $password;
	}
    
    public function login(){

        $username_get = $this->input->get('username');
        $password_get = $this->input->get('password');
        
       // login logic from training
        $username = $this->security->xss_clean($username_get);
        $password = $this->security->xss_clean($password_get);
		
		//echo $username. " :  " .  $password. " : " . md5($password);
		
		if($username != '' && $password  != ''){
			
			
			// Validate the user can login
            $result = $this->MobileUserModel->mobile_validate($username, $password);
            // $result_userdata = $result->result_array();
			//echo $result;
			// Now we verify the result
		    
    
                    // $result = $this->auth_model->login($data);
                    $user_data = array(
                         'user_id' => $result['user_id'],
                         'user' => $result['user'],
                         'email' => $result['email'],
                         'cost_center_code' => $result['cost_center_code'],
                         'cost_description' => $result['cost_description']
                
                    );
    
                    // echo json_encode($data);
                    // echo json_encode($result);
    
                    if ($result) {
                        echo json_encode($user_data);
                    } else {
                        echo json_encode($result);
                    }

		} 
       //       
    }

    public function create_new_count_no(){
		
		// get parameter from mobile
        // $request_type = $this->input->get('request_type');
        // $user_id = $this->input->get('user_id');
        $user_create_count_no = $this->input->get('user_id');
        $costcenter_code =  $this->input->get('costcenter_code');
        
        
        $ref_uniqid = uniqid(); 
        
        $submit_status = '1';


        $data = array(
            // 'req_type' => $request_type,
            'costcenter_code' => $costcenter_code,
            'submit_status ' => $submit_status,
            //'center_code' => $center_code,
            'user_create_count_no' => $user_create_count_no,
            'ref_uniqid' => $ref_uniqid
            );


        // print_r($data);
        // insert t_transaction    
        $result = $this->Count_model->insert_count_header($data);
        
        if($result){
			
                $item = $this->Count_model->get_count_by_uniqid($ref_uniqid);
                
                $req_number = $item[0]['count_no'];

				$array = array('code'=> 'Success', 'msg'=> $req_number.' create successfully!');
				$result = json_encode($array);

                echo $result;
            //echo "Issue Successfully!";
        }
        else {
				$array = array('code'=> 'Error', 'msg'=> 'Can not save data');
				$result = json_encode($array);

                echo $result;
        }   
    } 

    public function add_asset_to_count_no(){

        $user_id = $this->input->get('user_id');
        $costcenter_code =  $this->input->get('costcenter_code');
        $count_no = $this->input->get('count_no');
        $qr_code_id = $this->input->get('qr_code');

        // TH601500200001
        
        // $data = array(
        //             'user_id' => $user_id,
        //             'costcenter_code' => $costcenter_code,
        //             'count_no' => $count_no,
        //             // 'it_asset_check' => $it_asset,
        //             'qr_code_id' => $qr_code_id
        //             );

        // print_r($data) ;
        // die();

        // check it asset 
        // $it_asset_detail = $this->Fix_asset_model->check_it_asset($qr_code_id);
 
        // if ($it_asset_detail == false) {
        //     $asset_check = 'n' ;
        // } else {
        //     $asset_check = 'y' ;
        //     $it_asset = $it_asset_detail[0]['asset_id'];
        //     $asset_location = $it_asset_detail[0]['service_center'];
        // }

        //  get asset detail by qrcode 
        $asset_detail = $this->Fix_asset_model->get_asset_by_qrcode($qr_code_id);
 
        if ($asset_detail == false) {
            $asset_id = '' ;
            $asset_check = 'n' ;
        } else {
            $asset_check = 'y' ;
            $asset_id = $asset_detail[0]['asset_id'];
            $book_value = $asset_detail[0]['book_value'];
            // $asset_location = $it_asset_detail[0]['service_center'];
        }


        // check user role
        $user_detail = $this->MobileUserModel->get_user_role($user_id);
       
        if ($user_detail == false) {
            $user_role = '';
        } else {
            $user_role = $user_detail[0]['role'];
        }

        // print_r($user_detail);

                // เช็คว่า Asset ที่ scan มาตรงกับ costcenter ของ user หรือไม่
                // check user location 
                // $user_loc_detail = $this->Auth_model->get_location($id);

                // if ($user_loc_detail == false) {
                //     $user_loc = '';
                // } else {
                //     $user_loc = $user_loc_detail[0]['sc_id'];
                // }

        // check duplicate asset in request เช็คว่ามี Asset ในรายการแล้วหรือไม่ Scan ซ้ำ
        $count_asset_detail = $this->Count_model->get_duplicate_asset_in_count($count_no, $asset_id);
        if ($count_asset_detail == false) {
           $duplicated = 'n';
        } else {
            $duplicated = 'y';
        }
       
        // $data = array(
        //             'user_id' => $id,
        //             'request_no' => $reqest_no,
        //             'qr_code' => $qr_code_id,
        //             // 'it_asset_check' => $it_asset,
        //             'user_role' => $user_role
        //             );

        // // print_r($data) ;

        if ($asset_check == 'n'){ // qrcode นี ไม่มี asset บนระบบ
            $array = array('code'=> 'Warning', 'msg'=> 'Not found Asset Data!');
            $result = json_encode($array);
            echo $result;
        // } else if($user_role == 2 and $it_asset == 1){
        //     $array = array('code'=> 'Warning', 'msg'=> 'You are location admin this asset is IT Asset !');
        //     $result = json_encode($array);
        //     echo $result;
        // } else if ($user_role == 2 and $asset_location !== $user_loc) {
        //     $array = array('code'=> 'Warning', 'msg'=> 'Asset is '.$asset_location.' your service center is '.$user_loc);
        //     $result = json_encode($array);
        //     echo $result;
        // } else if($user_role == 4 and $it_asset == 0){
        //     $array = array('code'=> 'Warning', 'msg'=> $reqest_no.'You are IT admin this asset is not IT Asset !');
        //     $result = json_encode($array);
        //     echo $result;
        }else if ($duplicated == 'y'){ // มี Asset ในรายการแล้ว Scan ซ้ำ
            $array = array('code'=> 'Warning', 'msg'=> 'Asset is already exits in this count !');
            $result = json_encode($array);
            echo $result;
        } else {
            // Passed check all condition start to insert ผ่านทุกเงื่อนไข เตรียม บันทึกข้อมูลใน count detail 
            // function for find asset_id and book_value
            // $asset_detail = $this->Fix_asset_model->get_item_asset_detail($qr_code_id);

            // $asset_id = $asset_detail[0]['asset_id'];
            
            $data = array(
                // 'user_id' => $id,
                'count_no' => $count_no,
                // 'qr_code' => $qr_code_id,
                'asset_id' => $asset_id,
                'book_value' => $book_value
                );
            
            $result = $this->Count_model->insert_count_detail($data);

            $array = array('code'=> 'Success', 'msg'=> 'Add asset to count no. '.$count_no.' successfully!');
            $result = json_encode($array);
            echo $result;
        }

        
    }

    public function get_count_list_by_user(){
	    //$barcode = $this->input->get('barcode_text');
			$user_id = $this->input->get('user_id');
			// $center_code = $this->input->get('center_code');
		
			// $data = array(
			// 'user_request' => $user_id
            // // 'center_code' => $center_code
            // );
				$result = $this->Count_model->get_count_by_user($user_id);		
				$res =  json_encode($result);
                echo $res;

					
    }

    public function get_asset_list_by_count_no(){
	    //$barcode = $this->input->get('barcode_text');
			    $count_no = $this->input->get('count_no');
			
				$result = $this->Count_model->get_asset_list_by_count_no($count_no);		
				$res =  json_encode($result);
                echo $res;

					
    }

    public function get_asset_detail(){

        $asset_id = $this->input->get('asset_id');

        $result = $this->Fix_asset_model->get_item_by_id($asset_id);		
		$res =  json_encode($result);
        echo $res;
    }

    public function delete_asset_from_count_list(){
	  
        $count_detail_id = $this->input->get('count_detail_id');
    
        $result_model = $this->Count_model->delete_asset_from_count_list($count_detail_id);		

            $array = array('code'=> 'Success', 'msg'=> 'Delete Asset successfully!');
            $result = json_encode($array);

            echo $result;
    }

    public function delete_count(){
	  
        $count_no = $this->input->get('count_no');
    
        // $submit_status = '2';
                
        $result_detail = $this->Count_model->delete_count_detail($count_no);	

        $result_header = $this->Count_model->delete_count_header($count_no);	

        $array = array('code'=> 'Warning', 'msg'=> 'Delete Count complete !');
        $result = json_encode($array);

        echo $result;
        
                       
    }

    public function submit_count(){
	  
        $count_no = $this->input->get('count_no');
    
        // $submit_status = '2';

        // update submit status = 2        
        $result_model = $this->Count_model->submit_count($count_no);	

        if($result_model == true){

            $array = array('code'=> 'Success', 'msg'=> 'Submit Count Successfully!');
            $result = json_encode($array);
            echo $result;
            
            // get approve list by select from v_flow_approve
            // $result_list = $this->Request_model->insert_approve_list($req_number);
                       
            //     if($result_list == true){

            //         $array = array('code'=> 'Success', 'msg'=> 'Submit Count Successfully!');
            //         $result = json_encode($array);
            
            //         // curl
            //                 // Get cURL resource
            //                     $curl = curl_init();
            //                     // Set some options - we are passing in a useragent too here
            //                     curl_setopt_array($curl, array(
            //                         CURLOPT_RETURNTRANSFER => 1,
            //                         // CURLOPT_URL => 'https://service-imsp.com/dhl-fa-v3/api/send_email/alert_approver/'.$req_number.'',
            //                         CURLOPT_URL => 'https://service-imsp.com/dhl-fa-v3/api/send_email/alert_admin/'.$req_number.'',
            //                         CURLOPT_USERAGENT => 'Codular Sample cURL Request'
            //                     ));
            //                     // Send the request & save response to $resp
            //                     $resp = curl_exec($curl);
            //                     // Close request to clear up some resources
            //                     curl_close($curl);
                                
            //                     // echo $curl;
            //                     // echo $resp;
            //         // end of curl

            //         echo $result;

            //     } else {
            //         $array = array('code'=> 'Warning', 'msg'=> 'Request status is already submit!');
            //         $result = json_encode($array);
            
            //         echo $result;
            //         // $next = base_url("api/send_email/alert_approver/$req_number");
            //         // echo $next;               
            //     }

            }else {
                $array = array('code'=> 'Warning', 'msg'=> 'can not create approve list !');
                $result = json_encode($array);
        
                echo $result;
            }  

    }


//////////////////////////////////////////////////

    public function getcostcenter(){
	    // $item_barcode = $this->input->get('barcode_text');
		// $whse_center_code = $this->input->get('whse');
    
        // $data = array(
        //     'item_barcode' => $item_barcode,
        //     'whse_center_code' => $whse_center_code 
        //     );
	
				$result = $this->Service_center_model->get_active_costcenter();
	
                echo json_encode($result);
    }

    public function get_request_list_by_user(){
	    //$barcode = $this->input->get('barcode_text');
			$user_id = $this->input->get('user_id');
			// $center_code = $this->input->get('center_code');
		
			// $data = array(
			// 'user_request' => $user_id
            // // 'center_code' => $center_code
            // );
				$result = $this->Request_model->get_request_by_user($user_id);		
				$res =  json_encode($result);
                echo $res;

					
    }
    
    public function get_approve_list_by_req_num(){
	    //$barcode = $this->input->get('barcode_text');
			$req_no = $this->input->get('req_num');
			
				$result = $this->Request_model->get_approve_list($req_no);	
				$res =  json_encode($result);
                echo $res;

					
    }

    public function get_asset_list_by_req_num(){
	    //$barcode = $this->input->get('barcode_text');
			    $req_num = $this->input->get('req_num');
			
				$result = $this->Request_model->get_asset_list_by_req_num($req_num);		
				$res =  json_encode($result);
                echo $res;

					
    }

    public function delete_asset_from_list(){
	  
			    $request_detail_id = $this->input->get('request_detail_id');
			
				$result_model = $this->Request_model->delete_asset_from_list($request_detail_id);		
                
            //     if($result_model){
			
                    // $array = array('code'=> 'Success', 'msg'=> $request_detail_id.' Delete successfully!');
                    $array = array('code'=> 'Success', 'msg'=> 'Delete successfully!');
                    $result = json_encode($array);
    
                    echo $result;
            //     //echo "Issue Successfully!";
            // }
            // else {
            //         $array = array('code'=> 'Error', 'msg'=> 'Can not delete asset');
            //         $result = json_encode($array);
    
                    // echo $result_model;
            // }   
               				
    }   

    public function submit_request(){
	  
        $req_number = $this->input->get('req_number');
    
        // $submit_status = '2';

        // update submit status = 2        
        $result_model = $this->Request_model->submit_request($req_number);	

        if($result_model == true){
            
            // select from v_flow_approve
            $result_list = $this->Request_model->insert_approve_list($req_number);
            
            if($result_list == true){

                $array = array('code'=> 'Success', 'msg'=> 'Submit request successfully!');
                $result = json_encode($array);
        
                // curl
                        // Get cURL resource
                            $curl = curl_init();
                            // Set some options - we are passing in a useragent too here
                            curl_setopt_array($curl, array(
                                CURLOPT_RETURNTRANSFER => 1,
                                // CURLOPT_URL => 'https://service-imsp.com/dhl-fa-v3/api/send_email/alert_approver/'.$req_number.'',
                                CURLOPT_URL => 'https://service-imsp.com/dhl-fa-v3/api/send_email/alert_admin/'.$req_number.'',
                                CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                            ));
                            // Send the request & save response to $resp
                            $resp = curl_exec($curl);
                            // Close request to clear up some resources
                            curl_close($curl);
                            
                            // echo $curl;
                            // echo $resp;
                // end of curl

                echo $result;

            } else {
                $array = array('code'=> 'Warning', 'msg'=> 'Request status is already submit!');
                $result = json_encode($array);
        
                echo $result;
                // $next = base_url("api/send_email/alert_approver/$req_number");
                // echo $next;               
            }

        }else {
            $array = array('code'=> 'Warning', 'msg'=> 'can not create approve list !');
            $result = json_encode($array);
    
            echo $result;
        }

       
        
                       
    }

    public function delete_request(){
	  
        $req_number = $this->input->get('req_number');
    
        // $submit_status = '2';

                
        $result_detail = $this->Request_model->delete_request_detail($req_number);	

        $result_header = $this->Request_model->delete_request_header($req_number);	

        $array = array('code'=> 'Warning', 'msg'=> 'Delete request complete !');
        $result = json_encode($array);
       
        
                       
    }
    
    public function create_new_request(){
		
		// get parameter from mobile
		$request_type = $this->input->get('request_type');
		$to_costcenter = $this->input->get('to_costcenter');
		$center_code =  $this->input->get('center_code');
        $username = $this->input->get('username');
        $user_id = $this->input->get('user_id');

        $ref_uniqid = uniqid(); 
        
        $submit_status = '1';


        $data = array(
            'req_type' => $request_type,
            'to_costcenter' => $to_costcenter,
            'submit_status ' => $submit_status,
            //'center_code' => $center_code,
            'user_request' => $user_id,
            'ref_uniqid' => $ref_uniqid
            );


        // print_r($data);
        // insert t_transaction    
        $result = $this->Request_model->insert_request_header($data);
        
        if($result){
			
                $item = $this->Request_model->get_request_by_uniqid($ref_uniqid);
                
                $req_number = $item[0]['req_number'];

				$array = array('code'=> 'Success', 'msg'=> $req_number.' create successfully!');
				$result = json_encode($array);

                echo $result;
            //echo "Issue Successfully!";
        }
        else {
				$array = array('code'=> 'Error', 'msg'=> 'Can not save data');
				$result = json_encode($array);

                echo $result;
        }   
    } 

    public function add_asset_to_request(){

        $id = $this->input->get('user_id');
        $reqest_no = $this->input->get('req_number');
        $qr_code_id = $this->input->get('qr_code');

        // check it asset 
        $it_asset_detail = $this->Fix_asset_model->check_it_asset($qr_code_id);
 
        if ($it_asset_detail == false) {
            $asset_check = 'n' ;
        } else {
            $asset_check = 'y' ;
            $it_asset = $it_asset_detail[0]['asset_id'];
            $asset_location = $it_asset_detail[0]['service_center'];
        }

        // check user role
        $user_detail = $this->Auth_model->get_user_role($id);
       
        if ($user_detail == false) {
            $user_role = '';
        } else {
            $user_role = $user_detail[0]['role'];
        }


        // check user location 
        $user_loc_detail = $this->Auth_model->get_location($id);

        if ($user_loc_detail == false) {
            $user_loc = '';
        } else {
            $user_loc = $user_loc_detail[0]['sc_id'];
        }

        // check duplicate asset in request
        $req_asset_detail = $this->Request_model->get_duplicate_asset_in_request($reqest_no, $it_asset);
        if ($req_asset_detail == false) {
           $duplicated = 'n';
        } else {
            $duplicated = 'y';
        }
       

        $data = array(
                    'user_id' => $id,
                    'request_no' => $reqest_no,
                    'qr_code' => $qr_code_id,
                    // 'it_asset_check' => $it_asset,
                    'user_role' => $user_role
                    );

        // print_r($data) ;

        if ($asset_check == 'n'){
            $array = array('code'=> 'Warning', 'msg'=> 'Not found Asset !');
            $result = json_encode($array);
            echo $result;
        } else if($user_role == 2 and $it_asset == 1){
            $array = array('code'=> 'Warning', 'msg'=> 'You are location admin this asset is IT Asset !');
            $result = json_encode($array);
            echo $result;
        } else if ($user_role == 2 and $asset_location !== $user_loc) {
            $array = array('code'=> 'Warning', 'msg'=> 'Asset is '.$asset_location.' your service center is '.$user_loc);
            $result = json_encode($array);
            echo $result;
        } else if($user_role == 4 and $it_asset == 0){
            $array = array('code'=> 'Warning', 'msg'=> $reqest_no.'You are IT admin this asset is not IT Asset !');
            $result = json_encode($array);
            echo $result;
        }else if ($duplicated == 'y'){
            $array = array('code'=> 'Warning', 'msg'=> 'Asset is already exits in this request !');
            $result = json_encode($array);
            echo $result;
        } else {
            // Passed check all condition start to insert
            // function for find asset_id and book_value
            $asset_detail = $this->Fix_asset_model->get_item_asset_detail($qr_code_id);

            $asset_id = $asset_detail[0]['asset_id'];
            $net_value = $asset_detail[0]['book_val'];
            $data = array(
                // 'user_id' => $id,
                'request_no' => $reqest_no,
                // 'qr_code' => $qr_code_id,
                'asset_id' => $asset_id,
                'net_value' => $net_value
                );
            
            $result = $this->Request_model->insert_request_detail($data);

            $array = array('code'=> 'Success', 'msg'=> 'Add asset to request no. '.$reqest_no.' successfully!');
            $result = json_encode($array);
            echo $result;
        }

        
    }

}
