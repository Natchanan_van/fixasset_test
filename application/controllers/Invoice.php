<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class Invoice extends CI_Controller {
 	private $tbl_name = 'Invoice';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct(); 
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('invoice/invoice_view'); 
		$this->load->view('share/footer');
	}
	 
	
	public function addInvoice() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('InvoiceModel','',TRUE);
			$this->load->model('InvoiceDetailModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['IssueDate'] =  isset($dataPost['IssueDate'])?$dataPost['IssueDate']: "";
			$data['IssueOrder'] = isset($dataPost['IssueOrder'])?$dataPost['IssueOrder']: "";
			$data['pro_id'] = isset($dataPost['pro_id'])?$dataPost['pro_id']: "";
			$data['cus_id'] = isset($dataPost['cus_id'])?$dataPost['cus_id']: "";
			$data['cus_name'] = isset($dataPost['cus_name'])?$dataPost['cus_name']: "";
			$data['due_date'] = isset($dataPost['due_date'])?$dataPost['due_date']: "";
			$data['cus_contact'] = isset($dataPost['cus_contact'])?$dataPost['cus_contact']: "";
			$data['cus_tel'] = isset($dataPost['cus_tel'])?$dataPost['cus_tel']: "";
			$data['cus_address'] =  isset($dataPost['cus_address'])?$dataPost['cus_address']: "";
			
			$data['com_contact'] = isset($dataPost['com_contact'])?$dataPost['com_contact']: "";
			$data['com_tel'] = isset($dataPost['com_tel'])?$dataPost['com_tel']: "";
			$data['com_address'] = isset($dataPost['com_address'])?$dataPost['com_address']: "";
			$data['com_email'] = isset($dataPost['com_email'])?$dataPost['com_email']: "";
			$data['sub_total'] = isset($dataPost['sub_total'])?str_replace("," , "" , $dataPost['sub_total']): "";
			$data['vat'] =  isset($dataPost['vat'])?str_replace("," , "" , $dataPost['vat']): 0;
			$data['total'] =  isset($dataPost['total'])?str_replace("," , "" , $dataPost['total']): 0;
			
			$data['sub_alphabet'] = isset($dataPost['sub_alphabet'])?$dataPost['sub_alphabet']: "";
			$data['sign_name'] = isset($dataPost['sign_name'])?$dataPost['sign_name']: "";
			$data['sign_date'] = isset($dataPost['sign_date'])?$dataPost['sign_date']: "";
			
			$data['payment'] =  isset($dataPost['payment'])?$dataPost['payment']: "";
			
			$detail = isset($dataPost['detail'])?$dataPost['detail']: ""; 
			$master_id = $data['id'];
			
			 
			 
    		if ($data['id'] == 0) {  
    			$nResult = $this->InvoiceModel->insert($data);
				$master_id = $nResult;
		    }
		    else {  
		      	$nResult = $this->InvoiceModel->update($data['id'], $data);
		    }
			
			if(is_array($detail) && $master_id > 0){
				$nDetail = $this->InvoiceDetailModel->listUpdate($master_id,  $detail);
			}
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteInvoice(){
		try{
			$this->load->model('InvoiceModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->InvoiceModel->deleteInvoice($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getInvoiceDetailList(){
	 
		try{ 
			$this->load->model('InvoiceDetailModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$id =  isset($dataPost['id'])?$dataPost['id']: 0; 
			 
			$result['status'] = true;
			$result['message'] = $this->InvoiceDetailModel->getInvoiceDetailListById($id); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getInvoiceModelList(){
	 
		try{ 
			$this->load->model('InvoiceModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->InvoiceModel->getInvoiceModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->InvoiceModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getInvoiceModel(){
	 
		try{
			$this->load->model('InvoiceModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;    	
			  
			$result['status'] = true;
			$result['message'] = $this->InvoiceModel->getInvoiceModel($id); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getInvoiceComboList(){
	 
		try{ 
			$this->load->model('InvoiceModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->InvoiceModel->getInvoiceComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function printPDF(){
		//$this->pdi();
		
		define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		require(APPPATH .'fpdf/fpdf.php'); 
		require_once(APPPATH .'fpdi/autoload.php');
		 
		//echo "Dir :".APPPATH.'/../materia/Invoice_Template.pdf'; return;
		
		try {
			$this->load->model('CompanyModel','',TRUE);
			$this->load->model('CustomerModel','',TRUE);
			$this->load->model('InvoiceModel','',TRUE);
			$this->load->model('InvoiceDetailModel','',TRUE);
			
			$id = isset($_GET['id'])?$_GET['id']: 0;
			
			$companyDatas = $this->CompanyModel->getCompanyModel();
			$masterDatas = $this->InvoiceModel->getInvoiceModel($id);
			$detailData = $this->InvoiceDetailModel->getInvoiceDetailListById($id);
			
			
			$masterData = $masterDatas[0];
			$companyData = $companyDatas[0];
			
			$customerDetail = $this->CustomerModel->getCustomerModel($masterData['cus_id']);
			
			//print_r($customerDetail);
			
			$filename = APPPATH.'/../materia/Invoice_Template.pdf';
			$filename2 = APPPATH.'/../materia/Invoice_Template2.pdf';
			$pdf_name = $masterData['IssueOrder'].".pdf"; 
			$pdf = new  FPDI('p','mm','A4');			
			$pdf -> AddPage(); 

			$pdf->setSourceFile($filename); 
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			// now write some text above the imported page
			
			$pdf->AddFont('AngsanaNew','','angsa.php');
			$pdf->AddFont('AngsanaNew','B','angsab.php');
			$pdf->AddFont('AngsanaNew','I','angsai.php');
			$pdf->SetFont('AngsanaNew','',12);
			
			//$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);
			
			 
			$IssueDate = new DateTime($masterData['IssueDate']);
			$sign_date = new DateTime($masterData['sign_date']);
			
			$tab1 = 22;
			$tab2 = 24;
			$tab3 = 32;
			$tab3Ex = 80;
			$tab4 = 126;
			$tab5 = 150;
			$tab6 = 175;
			$tabEnd = 160;
			$lineStart = 25;
			$lineBr = 4;
			

			// Write something
			//บรรทัด 1
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $companyData['name']));
			$lineStart += $lineBr;
			
			//บรรทัด 2
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['com_address'])); 
			$lineStart += $lineBr;
			
			//บรรทัด 3
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'โทร.  '. $masterData['com_tel']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Date. '. $IssueDate->format('d-m-Y'))); 
			$lineStart += $lineBr;
			
			//บรรทัด 4
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  'เลขประจำตัวผู้เสียภาษี. '. $companyData['taxid']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Invoice #. '.  $masterData['IssueOrder']));
			$lineStart += $lineBr;
			
			//บรรทัด 5
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Customer Code. '.  $customerDetail[0]['code'])); 
			$lineStart += $lineBr;
			
			//บรรทัด 6
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'ติดต่อ. '. $masterData['com_contact']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Due date. '. $masterData['due_date']));
			$lineStart += $lineBr;
			
			//บรรทัด 7
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'โทร. '. $masterData['com_tel']. ' email. '.  $masterData['com_email']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$lineStart += $lineBr;
			
			//บรรทัด 8
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$lineStart += (2 + $lineBr);
			
			//บรรทัด 9 
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['cus_name'] ));
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'ติดต่อ. '. $masterData['cus_contact']));
			$lineStart += $lineBr;
			
			//บรรทัด 10
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   'เลขประจำตัวผู้เสียภาษี. '.$customerDetail[0]['taxid'] ));
			$lineStart += 4;
			
			//บรรทัด 11
			$pdf->SetXY($tab1, $lineStart );
			//$pdf->drawTextBox('This sentence is centered in the middle of the box.', 50, 50, 'C', 'M');
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['cus_address']  ) , 0);
			$lineStart += (7*$lineBr);
			
			//print_r($detailData );
			//บรรทัด 12
			  
			foreach($detailData  as $key => $value){
				
				$pdf->SetXY($tab2, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['item_no'] ));
				$pdf->SetXY($tab3, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['item_desc'] ));
				$pdf->SetXY($tab4, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['qty'].' '.$value['unit'] ));
				$pdf->SetXY($tab5, $lineStart );
				if($value['price'] > 0){
					//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['price'] ));
					$pdf->Cell(10,0,number_format($value['price'], 2, '.', ','),0,0,'R');
				}
				$pdf->SetXY($tab6, $lineStart );
				if($value['amount'] > 0){
					//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['amount'] ));
					$pdf->Cell(10,0,number_format($value['amount'], 2, '.', ','),0,0,'R');
				}
				 
				$lineStart += $lineBr;
			}
			
		    $lineCount = count($detailData );
			
			while($lineCount < 25){
				$lineCount++;
				$lineStart += $lineBr;
			}
			
			$lineStart +=2;
			$pdf->SetXY($tab6, $lineStart ); 
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['sub_total'] ));
			$pdf->Cell(10,0,number_format($masterData['sub_total'], 2, '.', ','),0,0,'R');
			$lineStart += $lineBr;
			$lineStart += $lineBr;
			
			$pdf->SetXY($tab6, $lineStart );
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['vat'] ));
			$pdf->Cell(10,0,number_format($masterData['vat'], 2, '.', ','),0,0,'R');
			$lineStart += $lineBr + 1; 
			
			$pdf->SetXY($tab6, $lineStart );
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['total'] ));
			$pdf->Cell(10,0,number_format($masterData['total'], 2, '.', ','),0,0,'R');
			$lineStart += (2*$lineBr) ;
			
			
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['sub_alphabet'] ) , 0);
			$lineStart += (4*$lineBr);
			
			$pdf->SetXY($tab1, $lineStart ); 
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['payment'] ) , 0);
			$lineStart += (4*$lineBr);
			
			$lineStart -= ((4*$lineBr) + 1);
			$pdf->SetXY($tab5, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['sign_name'] ));
			$lineStart += $lineBr + 1;
			$pdf->SetXY($tab5, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $sign_date->format('d-m-Y') ));
			
			
			//add copy page
			$pdf -> AddPage();
			$pdf->setSourceFile($filename2);
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			
			$pdf->AddFont('AngsanaNew','','angsa.php');
			$pdf->AddFont('AngsanaNew','B','angsab.php');
			$pdf->AddFont('AngsanaNew','I','angsai.php');
			$pdf->SetFont('AngsanaNew','',12);
			
			//$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);
			
			 
			$IssueDate = new DateTime($masterData['IssueDate']);
			$sign_date = new DateTime($masterData['sign_date']);
			
			$tab1 = 22;
			$tab2 = 24;
			$tab3 = 32;
			$tab3Ex = 80;
			$tab4 = 130;
			$tab5 = 147;
			$tab6 = 175;
			$tabEnd = 160;
			$lineStart = 25;
			$lineBr = 4;
			

			// Write something
			//บรรทัด 1
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $companyData['name']));
			$lineStart += $lineBr;
			
			//บรรทัด 2
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['com_address'])); 
			$lineStart += $lineBr;
			
			//บรรทัด 3
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'โทร.  '. $masterData['com_tel']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Date. '. $IssueDate->format('d-m-Y'))); 
			$lineStart += $lineBr;
			
			//บรรทัด 4
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  'เลขประจำตัวผู้เสียภาษี. '. $companyData['taxid']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Invoice #. '.  $masterData['IssueOrder']));
			$lineStart += $lineBr;
			
			//บรรทัด 5
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Customer Code. '.  $customerDetail[0]['code'])); 
			$lineStart += $lineBr;
			
			//บรรทัด 6
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'ติดต่อ. '. $masterData['com_contact']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'Due date. '. $masterData['due_date']));
			$lineStart += $lineBr;
			
			//บรรทัด 7
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'โทร. '. $masterData['com_tel']. ' email. '.  $masterData['com_email']));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$lineStart += $lineBr;
			
			//บรรทัด 8
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , ''));
			$lineStart += (2 + $lineBr);
			
			//บรรทัด 9 
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['cus_name'] ));
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , 'ติดต่อ. '. $masterData['cus_contact']));
			$lineStart += $lineBr;
			
			//บรรทัด 10
			$pdf->SetXY($tab1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   'เลขประจำตัวผู้เสียภาษี. '.$customerDetail[0]['taxid'] ));
			$lineStart += 4;
			
			//บรรทัด 11
			$pdf->SetXY($tab1, $lineStart );
			//$pdf->drawTextBox('This sentence is centered in the middle of the box.', 50, 50, 'C', 'M');
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['cus_address'] ) , 0);
			$lineStart += (7*$lineBr);
			
			//print_r($detailData );
			//บรรทัด 12
			  
			foreach($detailData  as $key => $value){
				
				$pdf->SetXY($tab2, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['item_no'] ));
				$pdf->SetXY($tab3, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['item_desc'] ));
				$pdf->SetXY($tab4, $lineStart );
				$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['qty'].' '.$value['unit'] ));
				$pdf->SetXY($tab5, $lineStart );
				if($value['price'] > 0){
					//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['price'] ));
					$pdf->Cell(10,0,number_format($value['price'], 2, '.', ','),0,0,'R');
				}
				$pdf->SetXY($tab6, $lineStart );
				if($value['amount'] > 0){
					//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $value['amount'] ));
					$pdf->Cell(10,0,number_format($value['amount'], 2, '.', ','),0,0,'R');
				}
				 
				$lineStart += $lineBr;
			}
			
			$lineStart +=2;
			$pdf->SetXY($tab6, $lineStart );
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['sub_total'] ));
			$pdf->Cell(10,0,number_format($masterData['sub_total'], 2, '.', ','),0,0,'R');
			$lineStart += $lineBr;
			$lineStart += $lineBr;
			
			$pdf->SetXY($tab6, $lineStart );
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['vat'] ));
			$pdf->Cell(10,0,number_format($masterData['vat'], 2, '.', ','),0,0,'R');
			$lineStart += $lineBr + 1; 
			
			$pdf->SetXY($tab6, $lineStart );
			//$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['total'] ));
			$pdf->Cell(10,0,number_format($masterData['total'], 2, '.', ','),0,0,'R');
			$lineStart += (2*$lineBr) ;
			
			
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['sub_alphabet'] ) , 0);
			$lineStart += (4*$lineBr);
			
			$pdf->SetXY($tab1, $lineStart ); 
			$pdf->MultiCell( 100, 4, iconv( 'UTF-8','cp874' , $masterData['payment'] ) , 0);
			$lineStart += (4*$lineBr);
			
			$lineStart -= ((4*$lineBr) + 1);
			$pdf->SetXY($tab5, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $masterData['sign_name'] ));
			$lineStart += $lineBr + 1;
			$pdf->SetXY($tab5, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $sign_date->format('d-m-Y') ));
			 
			 
			
			// Output
			$pdf->Output($_SERVER["DOCUMENT_ROOT"].'/application/uploads/'. $pdf_name, 'I'); //D = download // I , F , S
			
		} catch (Exception $e) {
			echo "Dir :".__DIR__;
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	 
	public function pdf(){
		
		
		
		/*$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('Invoice/Invoice_view'); 
		$this->load->view('share/footer');
		*/
		/*return();
		//require('../fpdf/fpdf.php');*/
		
		/*$this->load->library('pdf');
		$pdf=new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(40,10,'Hello World!');
		$pdf->Output();*/
		
		
		  /*define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		  require(APPPATH .'fpdf/fpdf.php');*/
		  
		 
		  
		  $this->load->library('mypdf');  
		 
		  $pdf = new mypdf('p','mm','A4');
		  $pdf -> AddPage();
		 
		  $pdf -> setDisplayMode ('fullpage');
		 
		  $pdf -> setFont ('times','B',20);
		  $pdf -> cell(200,30,"Title",0,1);
		 
		  $pdf -> setFont ('times','B','20');
		  $pdf -> write (10,"Description");
		 
		  $pdf -> output ('your_file_pdf.pdf','D');
		

	}
 
	public function pdi()
	{
		
		define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		require(APPPATH .'fpdf/fpdf.php');
		//require(APPPATH .'fpdi/fpdfTpl.php');
		//require(APPPATH .'fpdi/fpdiTrait.php');
		//require(APPPATH .'fpdi/fpdi.php');
		require_once(APPPATH .'fpdi/autoload.php');
		
		
		try {
			$filename = APPPATH.'/../application/uploads/RefundWHT.PDF';
			$pdf_name = "test.pdf";
			//echo $filename .".......";
		   // $this->load->library('mypdi');  
		 
			$pdf = new Fpdi\FPDI('p','mm','A4');
			$pdf -> AddPage(); 

			$pdf->setSourceFile($filename); 
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			// now write some text above the imported page
			$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);

			// Write something
			$pdf->SetXY(88, 31);
			$pdf->Write(0, "Hello world");

			// Output
			$pdf->Output($_SERVER["DOCUMENT_ROOT"].'/application/uploads/'. $pdf_name, 'I'); //D = download // I , F , S
			
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}
