<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class Dashboard extends CI_Controller {
 	private $tbl_name = 'Dashboard';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
		
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	
	public function index()
	{
        $this->load->view('share/head');
        // $this->load->view('share/sidebar');
		$this->load->view('share/sidebar-fa');
		$this->load->view('dashboard/dashboard_view'); 
		$this->load->view('share/footer');
	}
}
 