<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('master_data/location_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addLocation() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('LocationModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['location_id'] =  isset($dataPost['location_id'])?$dataPost['location_id']: 0;
			$data['location_cat'] =  isset($dataPost['location_cat'])?$dataPost['location_cat']: 0;
			
	
			
			//print_r($data);
			
			
			$statusid = $this->LocationModel->checkLocationID($data['location_id']);
		
	  		// load model 
    		if ($statusid == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0;
    			$nResult = $this->LocationModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->LocationModel->update($data['location_id'], $data);
		    }
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteLocation(){
		try{
			$this->load->model('LocationModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$location_id =  isset($dataPost['location_id'])?$dataPost['location_id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->LocationModel->deleteLocation($location_id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function loadMaxLocationid(){

		$this->load->model('LocationModel','',TRUE); 

		$location_id = $this->LocationModel->maxLocationid();

		echo json_encode($location_id, JSON_UNESCAPED_UNICODE);
	}

	public function getLocationModelList(){
	 
		try{
			$this->load->model('LocationModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			   	
			$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;

			// print_r($dataModel);

			// $offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->LocationModel->getLocationModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->LocationModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			// print_r($result['message']);
		
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	

	
    public function getLocationComboList(){
	 
		try{ 
			$this->load->model('LocationModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->LocationModel->getLocationComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
