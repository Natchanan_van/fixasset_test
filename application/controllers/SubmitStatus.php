<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubmitStatus extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		// $this->load->view('share/head');
		// $this->load->view('share/sidebar-fa');
		// $this->load->view('master_data/account_view');
		// $this->load->view('share/footer');
	}
	

	
	
    public function getSubmitStatusComboList(){
	 
		try{ 
			$this->load->model('SubmitStatusModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->SubmitStatusModel->getSubmitStatusComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
