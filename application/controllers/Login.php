<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->library('session');
        //$this->lang->load("message","thai");
		
		/*if(!isset($_SESSION['u_user'])){
			redirect('/login', 'refresh');
		}*/
    }
	 
	public function index()
	{ 
		$data['msgError'] = '';
		$username = $this->security->xss_clean($this->input->post('uname'));
        $password = $this->security->xss_clean($this->input->post('pswsss'));
		
		//echo $username. " :  " .  $password. " : " . md5($password);
		
		if($username != '' && $password  != ''){
			
			$this->load->model('UserModel');
			// Validate the user can login
			$result = $this->UserModel->validate($username, $password);
			//echo $result;
			// Now we verify the result
			if(! $result){
				// If user did not validate, then show them login page again
				$data['msgError'] =  "User or Password incorrect!";
			}else{
				// If user did validate, 
				// Send them to members area
				// redirect('Dashboard');
				redirect('Fixasset');
			}  
		}
		
		$this->load->view('login/login_view', $data);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('/login', 'refresh');
	}
	 
}
