<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('master_data/account_view');
		$this->load->view('share/footer');
	}
	
	
	public function addAccount() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('AccountModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
			$data['account_id'] =  isset($dataPost['account_id'])?$dataPost['account_id']: 0;
			$data['account_code'] =  isset($dataPost['account_code'])?$dataPost['account_code']: 0;
			$data['account_name'] =  isset($dataPost['account_name'])?$dataPost['account_name']: 0;
			
			//print_r($data);
			
			$statusid = $this->AccountModel->checkAccountID($data['account_id']);
		
	  		// load model 
    		if ($statusid == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0;
    			$nResult = $this->AccountModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->AccountModel->update($data['account_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function loadMaxAccountid(){

		$this->load->model('AccountModel','',TRUE); 

		$account_id = $this->AccountModel->maxAccountid();

		echo json_encode($account_id, JSON_UNESCAPED_UNICODE);
	}
	
	public function deleteAccount(){
		try{
			$this->load->model('AccountModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$account_id =  isset($dataPost['account_id'])?$dataPost['account_id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->AccountModel->deleteAccount($account_id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	

	public function getAccountModelList(){
	 
		try{
			$this->load->model('AccountModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->AccountModel->getAccountModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->AccountModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CustomerModel->getCustomerModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getAccountModel(){
	 
		try{
			$this->load->model('AccountModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->AccountModel->getAccountNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->AccountModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->AccountModel->getAccountModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
    public function getAccountComboList(){
	 
		try{ 
			$this->load->model('AccountModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->AccountModel->getAccountComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
