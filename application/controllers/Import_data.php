<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import_data extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('customer/customer_view'); 
		$this->load->view('share/footer');
    }
    
    public function loadCostCenter(){

		$this->load->library('MyExcel');
		$this->load->model('CostCenterModel','',TRUE); 

		$objReader = PHPExcel_IOFactory::load('uploads/import/costcenter2.xlsx');
		$sheetData = $objReader->getActiveSheet()->toArray(true,true,true,true);

		foreach ($sheetData as $data) {
			try{
				$data_model['company_id'] =  isset($data['A'])?$data['A']: 0;
				$data_model['cost_code'] =  isset($data['B'])?$data['B']: 0;
				$data_model['cost_description'] =  isset($data['C'])?$data['C']: 0;
				$data_model['create_user'] =  "test";
                $data_model['update_user'] =  "test";
				
				// print_r($data_model);
				$nResult = $this->CostCenterModel->insert($data_model);

			}
			catch(Exception $ex){
				
				// $result['message'.$row_count] = $ex;
				echo $ex;
			}

		}
	
			
		echo "success";
    }
    
    public function loadAsset(){

		$this->load->library('MyExcel');
		$this->load->model('FixAssetModel','',TRUE); 

		$objReader = PHPExcel_IOFactory::load('uploads/import/asset2.xlsx');
		$sheetData = $objReader->getActiveSheet()->toArray(true,true,true,true);

		foreach ($sheetData as $data) {
			try{
				$data_model['company_id'] =  isset($data['A'])?$data['A']: 0;
				$data_model['asset_no'] =  isset($data['B'])?$data['B']: 0;
				$data_model['class_id'] =  isset($data['C'])?$data['C']: 0;
				$data_model['asset_description'] =   isset($data['D'])?$data['D']: 0;
                $data_model['cost_id'] =   isset($data['E'])?$data['E']: 0;
                $data_model['life'] =   isset($data['F'])?$data['F']: 0;
                $data_model['cap_date'] =   isset($data['G'])?$data['G']: 0;
                $data_model['ODep_start'] =   isset($data['H'])?$data['H']: 0;
                $data_model['acquisition'] =   isset($data['I'])?$data['I']: 0;
                $data_model['total'] =   isset($data['J'])?$data['J']: 0;	
                $data_model['book_value'] =   isset($data['K'])?$data['K']: 0;
                $data_model['account_id'] =   isset($data['L'])?$data['L']: 0;
                $data_model['doc_no'] =   isset($data['M'])?$data['M']: 0;
                $data_model['reference'] =   isset($data['N'])?$data['N']: 0;
                $data_model['location_id'] =   isset($data['O'])?$data['O']: 0;
                $data_model['qrcode'] =   isset($data['P'])?$data['P']: 0;
                $data_model['create_user'] =  "test";
                $data_model['update_user'] =  "test";
				// print_r($data_model);
				$nResult = $this->FixAssetModel->insert($data_model);

			}
			catch(Exception $ex){
				
				// $result['message'.$row_count] = $ex;
				echo $ex;
			}

		}
	
			
		echo "success";
	}
	
	
}
