<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FixAsset extends CI_Controller {
	public function __construct() {
		parent::__construct(); 
		echo $this->session->userdata('user_name');
		if(! $this->session->userdata('validated')){
            redirect('login');
		}
		
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('fixasset/fixasset_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addFixAsset() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('FixAssetModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['asset_id'] =  isset($dataPost['asset_id'])?$dataPost['asset_id']: 0;
			$data['asset_no'] =  isset($dataPost['asset_no'])?$dataPost['asset_no']: 0;
			$data['class_id'] =  isset($dataPost['class_id'])?$dataPost['class_id']: "";
			$data['asset_description'] = isset($dataPost['asset_description'])?$dataPost['asset_description']: "";
			$data['cost_id'] = isset($dataPost['cost_id'])?$dataPost['cost_id']: "";
			$data['life'] =  isset($dataPost['life'])?$dataPost['life']: "";
			$data['cap_date'] = isset($dataPost['cap_date'])?$dataPost['cap_date']: "";
			$data['ODep_start'] =  isset($dataPost['ODep_start'])?$dataPost['ODep_start']: "";
			$data['acquisition'] = isset($dataPost['acquisition'])?$dataPost['acquisition']: "";
			$data['total'] = isset($dataPost['total'])?$dataPost['total']: "";
			$data['book_value'] = isset($dataPost['book_value'])?$dataPost['book_value']: "0";
			$data['account_id'] =  isset($dataPost['account_id'])?$dataPost['account_id']: "";
			$data['doc_no'] =  isset($dataPost['doc_no'])?$dataPost['doc_no']: "";
			$data['reference'] =  isset($dataPost['reference'])?$dataPost['reference']: "";
			$data['location_id'] =  isset($dataPost['location_id'])?$dataPost['location_id']: "";
			$data['qrcode'] =  isset($dataPost['qrcode'])?$dataPost['qrcode']: "";
			// print_r($data);
			
			//$data['update_date'] = $dateRecord;
			//$data['update_user'] = $this->session->userdata('user_name'); 
	  		// load model 
    		if ($data['asset_id'] == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0; 
    			$nResult = $this->FixAssetModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->FixAssetModel->update($data['asset_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteFixAsset(){
		try{
			$this->load->model('FixAssetModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['asset_id'])?$dataPost['asset_id']:0;// $this->input->post('ap_id');

			// print_r($id);
			
			$bResult = $this->FixAssetModel->deleteFixAsset($id);
			//  print_r($bResult);
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getFixAssetModelList(){
	 
		try{
			$this->load->model('FixAssetModel','',TRUE); 
			   	
			/*$data['m_id'] =  $this->input->post('mab_m_id');
			
			$limit =  $this->input->post('limit');
			$offset =  $this->input->post('offset');
			$order = $this->input->post('order');
			$direction = $this->input->post('direction');
			 
			$result = $this->FixAssetModel->getFixAssetNameAllList($data, $limit , $offset, $order, $direction); */
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);

			$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

		
			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->FixAssetModel->getFixAssetNameAllList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->FixAssetModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);

				// print_r($result['message']); 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getFixAssetModel(){
	 
		try{
			$this->load->model('FixAssetModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			// print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->FixAssetModel->getFixAssetList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->FixAssetModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->FixAssetModel->getFixAssetModel(); 
			//  print_r($result['message']);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
    }
    
    public function printQrcode(){
        $asset_id = $this->uri->segment(3);

        // print_r($asset_id);

		$this->load->model('FixAssetModel','',TRUE); 
		// $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);
		$item = $this->FixAssetModel->get_item_by_id($asset_id);

		$asset_name = $item[0]['asset_no'];
		// $serial_no = $item[0]['invent_no'];
		$qrcode_img = $item[0]['qrcode'].".png";
		
        // print_r($qrcode_img);
		// $this->load->view("itasset/layout-qr_code.php", $data);

		//  echo(APPPATH .'fpdf/fpdf.php'); 
		require(APPPATH .'fpdf/Mypdf.php'); 
		
		  $pdf = new FPDF('p','mm', array(48,24));
		  $pdf = new Mypdf('p','mm', array(28,24));
		  $pdf->SetAutoPageBreak(false);
		  $pdf-> AddPage('L');
          
          $url = base_url('assets/images/qrcode/').$qrcode_img;
          $url_string = "'".$url."'";

        //   print_r($url_string);
        // Insert a dynamic image from a URL
        //   $pdf->Image('//localhost/dhl-fa-v2/assets/images/qrcode/5B50BEDAD1C57.png',2,3,18,0,'PNG');
        //   $pdf->Image($url_string,2,3,18,0,'PNG'); 
		  $pdf->Image(base_url().'assets/images/qrcode/'.$qrcode_img,2,3,18,0,'PNG');

		//   $pdf->SetXY(2, -5);
		//   $pdf->RotatedText(5,250,"Example of rotated text",90);

		  $pdf->SetFont('Arial','',5);
	      $pdf->SetXY(21,21);
		  $pdf->Rotate(90); 
		  $pdf->Cell(0,0,"AST:$asset_name",0,0,'L'); 

		  ob_start(); 
		  $pdf -> output ($_SERVER["DOCUMENT_ROOT"].'/assets/images/qrcode_pdf'. 'your_file_pdf.pdf','I');
		  ob_end_flush(); 

    }
	
	public function getFixAssetComboList(){
	 
		try{ 
			$this->load->model('FixAssetModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->FixAssetModel->getFixAssetComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
