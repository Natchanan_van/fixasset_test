<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CountAsset extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('count_asset/countasset_view');
		$this->load->view('share/footer');
	}
	
	
	public function addCountAsset() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('CountAssetModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
			$data['CountAsset_id'] =  isset($dataPost['CountAsset_id'])?$dataPost['CountAsset_id']: 0;
			$data['CountAsset_code'] =  isset($dataPost['CountAsset_code'])?$dataPost['CountAsset_code']: 0;
			$data['CountAsset_name'] =  isset($dataPost['CountAsset_name'])?$dataPost['CountAsset_name']: 0;
			
			//print_r($data);
			
			$statusid = $this->CountAssetModel->checkCountAssetID($data['CountAsset_id']);
		
	  		// load model 
    		if ($statusid == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0;
    			$nResult = $this->CountAssetModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->CountAssetModel->update($data['CountAsset_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function loadMaxCountAssetid(){

		$this->load->model('CountAssetModel','',TRUE); 

		$CountAsset_id = $this->CountAssetModel->maxCountAssetid();

		echo json_encode($CountAsset_id, JSON_UNESCAPED_UNICODE);
	}
	
	public function deleteCountAsset(){
		try{
			$this->load->model('CountAssetModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$CountAsset_id =  isset($dataPost['CountAsset_id'])?$dataPost['CountAsset_id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->CountAssetModel->deleteCountAsset($CountAsset_id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	

	public function getCountAssetModelList(){
	 
		try{
			$this->load->model('CountAssetModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CountAssetModel->getCountAssetModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CountAssetModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CustomerModel->getCustomerModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
    }
    
    public function getCountAssetDetailList(){
	 
		try{
			$this->load->model('CountAssetModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		// $PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			// $PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			// $direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			// $SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
            // $dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";
            $count_no = isset($dataPost['count_no'])?$dataPost['count_no']: "";

			// $offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CountAssetModel->getCountAssetDetailList($count_no );
			// $result['totalRecords'] = $this->CountAssetModel->getTotal($dataModel);
			// $result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			// print_r($result['message']);
			//$result['message'] = $this->CustomerModel->getCustomerModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getCountAssetModel(){
	 
		try{
			$this->load->model('CountAssetModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CountAssetModel->getCountAssetNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CountAssetModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			// print_r($result['message']);
			//$result['message'] = $this->CountAssetModel->getCountAssetModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
    public function getCountAssetComboList(){
	 
		try{ 
			$this->load->model('CountAssetModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->CountAssetModel->getCountAssetComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}

	
	public function  loadListCountAssetExcel(){
        $this->load->library('MyExcel'); 
		$this->load->model('CountAssetModel','',TRUE);  

		$dataPost = json_decode( $this->input->raw_input_stream , true);


		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
		$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
		$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
		$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
		$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

		$offset = ($PageIndex - 1) * $PageSize;
		// $start_date = $_GET['start_date'];
		// $end_date = $_GET['end_date'];
		// $start_date =  isset($dataPost['start_date'])?$dataPost['start_date']:"";
		// $end_date =  isset($dataPost['end_date'])?$dataPost['end_date']:"";
		// echo $start_date,$end_date;
		$objPHPExcel = new PHPExcel(); 
		// $emp_id = 'TH0065';
		$listData = $this->CountAssetModel->getCountAssetModelList($dataModel , $PageSize, $offset, $direction, $SortOrder);
		// print_r($listData); die();
		$objPHPExcel->getActiveSheet()->setTitle('List CountAsset Report');
		
		$objPHPExcel->setActiveSheetIndex(0);        
                                  
		// การจัดรูปแบบของ cell  
		$objPHPExcel->getDefaultStyle()  
								->getAlignment()  
								->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)  
								->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
								//HORIZONTAL_CENTER //VERTICAL_CENTER               
								 
		// จัดความกว้างของคอลัมน์
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);     
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);                                              
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
		// $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40); 
		// $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		// $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		// กำหนดหัวข้อให้กับแถวแรก
		$objPHPExcel->setActiveSheetIndex(0)  
					->setCellValue('A1', 'Count Asset No') 
					->setCellValue('B1', 'Cost Center Code') 
					->setCellValue('C1', 'Cost Center Description') 
					->setCellValue('D1', 'Count Asset Date') ;
					// ->setCellValue('E1', 'End Date') 
					// ->setCellValue('F1', 'Start Time') 
					// ->setCellValue('G1', 'End Time') 
					// ->setCellValue('H1', 'Location')    
					// ->setCellValue('I1', 'Room')  
					// ->setCellValue('J1', 'Deadline Register')  
					// ->setCellValue('K1', 'Minimum Trainee')
					// ->setCellValue('L1', 'Maximum Trainee')
					// ->setCellValue('M1', 'Class Status');
					$start_row=2;
					$i_num=0;
					foreach($listData as $row){
						// print_r($row);
						$i_num++;
						// $row['start_time'] = date($row['start_time'],'H:i');
						// $row['end_time'] = date($row['end_time'],'H:i');
						// เพิ่มข้อมูลลงแต่ละเซลล์                          
						$objPHPExcel->setActiveSheetIndex(0)  
									->setCellValue('A'.$start_row, $i_num)  
									->setCellValue('B'.$start_row, $row['count_no'])
									->setCellValue('C'.$start_row, $row['costcenter_code'])
									->setCellValue('D'.$start_row, $row['cost_description'])
									->setCellValue('E'.$start_row, $row['count_date']);
									// ->setCellValue('F'.$start_row, $row['start_time'])
									// ->setCellValue('G'.$start_row, $row['end_time'])
									// ->setCellValue('H'.$start_row, $row['location'])  
									// ->setCellValue('I'.$start_row, $row['room'])  
									// ->setCellValue('J'.$start_row, $row['deadline'])
									// ->setCellValue('K'.$start_row, $row['min_trainee'])
									// ->setCellValue('L'.$start_row, $row['max_trainee'])
									// ->setCellValue('M'.$start_row, $row['status_name']);            
									
						// เพิ่มแถวข้อมูล
						$start_row++;               
					}
		try{ 
		
			// $objReader->getActiveSheet()->getColumnDimension("T")->setAutoSize(TRUE);
			// $objReader->getActiveSheet()->getColumnDimension("U")->setAutoSize(TRUE);
			// $objReader->getActiveSheet()->getColumnDimension("V")->setAutoSize(TRUE);
		
			// $objReader->getActiveSheet()->getStyle('T5:V'. $rowCount)->applyFromArray($styleArray);
			// $objReader->getActiveSheet()->getStyle('T3:v3')->applyFromArray($styleBg);

			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
			$filename='listCountasset-'.date("dmYHi").'.xlsx';

			
			$objWriter->save(FCPATH.'uploads/count_asset/'.$filename); 

				
			// header('Content-Type: application/vnd.ms-excel'); 
			// header('Content-Disposition: attachment;filename="'.$filename); 
			// header('Cache-Control: max-age=0'); 
			// // $objWriter = PHPExcel_IOFactory::createWriter($objWriter, 'Excel5'); 
			// $objWriter->save('php://output');

			$result['status'] = true;
			
			
			
		}catch(Exception $ex){
			echo "exception: ".$ex;
		} 
		return  $result;
    }
}
