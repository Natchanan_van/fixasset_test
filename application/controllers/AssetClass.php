<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AssetClass extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('master_data/assetclass_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addAssetClass() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('AssetClassModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['class_id'] =  isset($dataPost['class_id'])?$dataPost['class_id']: 0;
			$data['class_code'] =  isset($dataPost['class_code'])?$dataPost['class_code']: 0;
			$data['class_description'] = isset($dataPost['class_description'])?$dataPost['class_description']: "";
			$data['it_asset'] = isset($dataPost['it_asset'])?$dataPost['it_asset']: "";
	
			// print_r($data);
			
		
			$statusid = $this->AssetClassModel->checkClassID($data['class_id']);
		
	  		// load model 
    		if ($statusid == 0) { 
    			$data['create_user'] = $this->session->userdata('user');
				$data['create_date'] = date("Y-m-d H:i:s");
				$data['delete_flag'] = 0;
    			$nResult = $this->AssetClassModel->insert($data);
		    }
		    else {  
				$data['update_user'] = $this->session->userdata('user');
				$data['update_date'] = date("Y-m-d H:i:s");
		      	$nResult = $this->AssetClassModel->update($data['class_id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function loadMaxClassid(){

		$this->load->model('AssetClassModel','',TRUE); 

		$cost_id = $this->AssetClassModel->maxClassid();

		echo json_encode($cost_id, JSON_UNESCAPED_UNICODE);
	}
	
	public function deleteAssetClass(){
		try{
			$this->load->model('AssetClassModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$class_id =  isset($dataPost['class_id'])?$dataPost['class_id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->AssetClassModel->deleteAssetClass($class_id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getAssetClassList(){
	 
		try{
			$this->load->model('AssetClassModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->AssetClassModel->getAssetClassModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->AssetClassModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CustomerModel->getCustomerModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
    public function getCostCenterComboList(){
	 
		try{ 
			$this->load->model('AssetClassModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->AssetClassModel->getCostCenterComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
